/*
 Navicat Premium Data Transfer

 Source Server         : mariadb-local-docker
 Source Server Type    : MariaDB
 Source Server Version : 100604
 Source Host           : localhost:3306
 Source Schema         : jwt

 Target Server Type    : MariaDB
 Target Server Version : 100604
 File Encoding         : 65001

 Date: 14/10/2021 01:24:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2020_09_28_131059_create_todos_table', 1);
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for todos
-- ----------------------------
DROP TABLE IF EXISTS `todos`;
CREATE TABLE `todos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `todos_created_by_foreign` (`created_by`),
  CONSTRAINT `todos_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of todos
-- ----------------------------
BEGIN;
INSERT INTO `todos` VALUES (1, 'Deleniti earum placeat libero aliquid.', 'Et quidem perspiciatis corrupti pariatur. Quis voluptatem voluptate dolorem facilis illo qui odio possimus. Voluptatem et aut non eligendi qui sed aspernatur.', 0, 6, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (2, 'Et voluptatem nisi porro.', 'Autem rem natus quod. Blanditiis ex nihil qui recusandae culpa. Qui ex occaecati consequuntur recusandae soluta dolore. Et laudantium sit provident neque nobis.', 1, 5, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (3, 'Eum quam facere voluptas quia.', 'Aut odio voluptas eaque facilis optio blanditiis. Provident minus itaque eos. Ipsam nesciunt est sapiente nihil a alias quos. Impedit asperiores voluptatibus voluptate voluptas reprehenderit asperiores quos. Quia harum sunt soluta nostrum ut.', 0, 10, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (4, 'Commodi aperiam velit beatae et voluptatum repellat.', 'Sit est hic expedita rerum hic optio. Soluta non nihil officiis. Quam molestiae pariatur quia voluptatem. Dolore ut facilis deleniti ipsa sed eum.', 1, 1, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (5, 'Necessitatibus sunt architecto enim nostrum omnis doloremque.', 'Omnis beatae ex aut repudiandae aut quae nobis. Qui et quod harum a velit qui voluptates. Libero consequatur consequuntur veniam eligendi hic suscipit. Numquam perferendis ut quis placeat sint explicabo.', 1, 7, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (6, 'Consequatur nihil vitae architecto.', 'Omnis saepe occaecati voluptates. Non adipisci molestiae sint beatae sequi. Modi aut et itaque eligendi repudiandae perspiciatis animi ducimus. Possimus dolore odit magnam enim aliquam blanditiis.', 0, 6, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (7, 'Sequi laborum et consectetur aut.', 'Culpa et nostrum qui qui. Harum culpa sunt quam dolorem recusandae et. Aut sunt mollitia ab dolorem rem totam enim. Iure aut et aut et.', 1, 4, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (8, 'Excepturi inventore ea nulla vitae et ut minus.', 'Iusto ut quo molestias fugit occaecati distinctio labore necessitatibus. Amet qui ut beatae dignissimos. Dolorem quibusdam architecto officia maiores minima fugiat. Et dolore aperiam et voluptatem ut blanditiis. Accusantium et cumque beatae.', 1, 2, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (9, 'Quam et nam velit autem officiis consequatur.', 'Officia molestias provident repellendus odit. Molestiae consectetur qui eos eveniet aperiam. Rem quaerat velit cumque qui. Ut quae odio in.', 0, 10, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (10, 'Iste non et mollitia et quia ut quis.', 'Non ea sit adipisci dolore doloremque deserunt. Amet magnam ullam et quas atque consectetur. Natus vel nihil laudantium. Nobis et est dicta explicabo quis.', 1, 10, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (11, 'Ad tempora nesciunt omnis nulla consequatur nobis sequi.', 'Aut nemo expedita culpa doloribus dolor architecto est. Blanditiis laborum incidunt dolorem ipsam. Quasi occaecati quo quas quia porro eos.', 1, 10, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (12, 'Commodi sed quidem amet architecto tempore nobis distinctio et.', 'Voluptatem qui impedit quisquam nulla aut nemo rerum. Libero totam pariatur incidunt totam sapiente fuga. Necessitatibus magni quia tenetur eveniet quis velit.', 1, 3, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (13, 'Culpa amet corporis et saepe dignissimos vitae.', 'Veritatis dolores sunt totam perspiciatis et. Velit incidunt cum est dignissimos consequatur voluptas. Qui eum molestiae non voluptas. Voluptatem ut dolores qui eos aperiam quibusdam nulla.', 0, 1, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (14, 'Aliquid minima nam maxime dicta voluptas aut voluptatibus.', 'Excepturi repellendus neque ut at nulla iste debitis officiis. Et perspiciatis et aliquid tempora. Vel et culpa quia quas optio.', 0, 1, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (15, 'Id in odit maxime ipsa.', 'Aut porro sed qui facilis consequuntur. Est illo ea consequatur dolores. Perspiciatis facere numquam delectus id natus iure voluptatum.', 0, 3, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (16, 'Est voluptas consequuntur ut molestias sed a tempore.', 'Modi deleniti sequi velit culpa adipisci repudiandae modi. In quia vel non qui laborum provident perspiciatis. Laborum qui dicta et occaecati et.', 1, 9, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (17, 'Deleniti consequatur provident magni quia rem quod.', 'Labore nam blanditiis rerum eos quo. Incidunt qui maiores fugiat eligendi sapiente incidunt. Deleniti quaerat quod animi saepe cupiditate. Facere rerum repellat quia consequatur recusandae.', 1, 3, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (18, 'Saepe esse nihil maiores recusandae dolor.', 'Perferendis sed veritatis est iure tenetur molestiae id. Omnis quas non est. Placeat eaque et voluptates fugiat enim cum.', 1, 1, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (19, 'Voluptatem ut odit aut unde.', 'Deserunt quidem qui nisi repellendus ratione exercitationem expedita. Est facere ut iure et reiciendis qui quo. Ut debitis ipsum maxime aut. Inventore odio eveniet quod facilis eos illum.', 0, 4, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (20, 'Autem architecto odit nam rerum deleniti.', 'Explicabo ut cumque ut. Recusandae omnis qui vero dolorem. Et inventore cumque est et. Consequatur asperiores delectus error qui. Ducimus id unde omnis eos ipsa voluptates et et.', 1, 9, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (21, 'Ullam sunt nemo dicta id culpa sit cupiditate aut.', 'Consequatur ut et et quia dolores distinctio. Doloribus vero autem omnis impedit. Vel et doloribus sit in id recusandae enim. Quaerat autem iste voluptas.', 1, 7, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (22, 'Vero recusandae soluta ipsam nesciunt doloribus aperiam quia.', 'Nihil esse minus commodi et architecto nihil est. Eius doloremque sit adipisci minima dolor sapiente. Saepe sed dolor doloremque doloremque aut aspernatur architecto. Est cum praesentium autem optio quam.', 0, 4, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (23, 'Aut non unde tenetur rerum autem ex.', 'Vel qui praesentium expedita ullam architecto iure. Delectus molestiae recusandae commodi voluptatem et. Aut unde earum non et. Quo quis quae dolorem qui dolorem fugiat. Vero assumenda nobis earum odit rerum aliquid non deleniti.', 0, 9, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (24, 'Autem temporibus magnam magni harum.', 'Ad nam rerum eaque animi. Aut delectus dignissimos dolorem quis voluptatem at quidem. Numquam distinctio neque autem voluptas.', 0, 6, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (25, 'Maxime dolorem doloremque accusamus debitis quia repellat sequi.', 'Vel aut sed et minima saepe voluptatem. Optio earum veritatis rerum et aut iste. Quia commodi consectetur et non aliquid sed. Tenetur animi voluptate repellat aut qui necessitatibus.', 1, 7, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (26, 'Officiis excepturi sed voluptas repudiandae.', 'Nesciunt voluptas commodi labore omnis. Aut pariatur voluptate et sit neque.', 1, 6, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (27, 'Est molestias sit ut repellat asperiores iure nobis.', 'Laboriosam perspiciatis voluptatem eveniet. Velit ipsam exercitationem facilis accusantium placeat nihil ipsa. Earum repellat est inventore vel qui aut quam ut. Ut at corrupti id.', 1, 8, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (28, 'Omnis veniam est quibusdam qui est est.', 'Laudantium quibusdam autem id odit eveniet eligendi asperiores. Vitae laboriosam illum ut est ullam non autem. Eaque non libero exercitationem ut qui velit. Commodi est quo ut et.', 0, 4, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (29, 'Possimus est aperiam rerum.', 'Repellendus ipsam mollitia natus aperiam. Praesentium est amet aliquam harum corrupti quo. Amet temporibus qui accusamus et odio nihil. Praesentium vel et et porro quia quisquam.', 0, 8, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (30, 'Eum explicabo accusantium aperiam eius et vitae.', 'Aut natus hic debitis. Incidunt tempore recusandae eos numquam similique enim eos. Vero at dolorem optio ad tempora voluptatibus accusamus et.', 1, 10, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (31, 'Molestiae voluptas sit ratione occaecati doloremque eum.', 'Error omnis qui aspernatur in iure autem sit. Vel quis molestiae voluptate. Quo impedit aut id neque placeat.', 0, 8, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (32, 'Consequatur fugiat animi recusandae iusto nihil laudantium est.', 'Neque et ex rem voluptas ea. Praesentium qui voluptatem totam accusantium. Hic soluta et nostrum adipisci ducimus dignissimos ad dignissimos.', 1, 1, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (33, 'Ea voluptatem voluptatem alias qui.', 'Aut iste a corrupti molestiae. Et tenetur alias nulla dolorem quia. Commodi quia non rerum vitae delectus nisi et. Suscipit provident iusto atque at omnis.', 0, 5, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (34, 'Mollitia error qui magni incidunt facere.', 'Facilis aspernatur molestiae nulla minus sed recusandae. Nam aut qui qui suscipit ullam. Reprehenderit maxime et nisi voluptatibus recusandae.', 0, 10, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (35, 'Tenetur dolor occaecati ipsum et.', 'Ut ut velit veritatis neque non. Laboriosam nostrum quae assumenda sequi omnis a ut dolorem.', 1, 9, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (36, 'Perferendis quia cupiditate ut ex quia nihil.', 'Exercitationem minus architecto eum soluta sit. Autem aliquid qui eligendi ipsum nam.', 1, 2, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (37, 'Explicabo et qui molestiae omnis.', 'Provident culpa aut et consequatur. Repudiandae illo ducimus ut voluptatem accusamus hic. Quia nobis nobis aut qui. Ullam quia facere est aut rerum nostrum nihil.', 1, 6, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (38, 'Sed hic facilis natus neque voluptatem reprehenderit inventore.', 'Culpa eum illo autem culpa expedita cupiditate tempore. Velit inventore sint esse nihil quia rerum vel. Voluptatem vel non ipsa recusandae occaecati qui voluptatibus.', 0, 5, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (39, 'Enim inventore facilis voluptate rem.', 'Necessitatibus magnam est nam rerum explicabo. Et cupiditate autem dolore eum dolor similique. Consequatur ut voluptatem voluptate a porro. Molestiae quia tempora quia optio perspiciatis. Est quibusdam voluptates reprehenderit omnis vero porro.', 0, 5, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (40, 'Accusantium saepe ipsum sequi eos sint et sint voluptate.', 'Vitae quod autem qui dolorem similique delectus. Et in perferendis pariatur eos corrupti. Eos cumque ea earum et ut illum. Qui quia odit asperiores doloremque.', 1, 6, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (41, 'In occaecati aut modi architecto harum fugiat nostrum.', 'Illo ipsa eius id iusto repellat ducimus iusto. Voluptatibus consequuntur voluptatem sunt est inventore. Ut consequatur inventore sit molestiae optio.', 1, 3, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (42, 'Aut sit et eligendi enim earum quisquam et.', 'Veritatis illo iste aut fugiat corrupti. Sunt magnam vitae aut id. Rerum explicabo rerum maxime rerum laudantium quo.', 0, 9, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (43, 'Aliquam deserunt repudiandae quo qui temporibus doloribus esse.', 'Debitis sed veritatis similique id non qui. Aut laudantium hic porro quae totam nihil reprehenderit. Aut tenetur voluptatem hic quia omnis non.', 0, 5, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (44, 'Dignissimos maxime atque et libero magnam natus et.', 'Impedit iusto harum reiciendis sunt vitae. Occaecati et quis aut nemo. Officia aspernatur quam modi voluptate eos non.', 0, 2, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (45, 'Tenetur voluptas maiores consectetur qui consectetur ut impedit.', 'Perferendis ratione quam autem. Nihil repellendus sit officia itaque enim. Fugit nam quae ratione.', 0, 8, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (46, 'Incidunt dolorem hic aut officia debitis quia.', 'Nesciunt ipsum aut sit quibusdam harum accusamus. Neque vitae non assumenda neque. Sit est ut fugit id neque. Tempora officiis rem sequi numquam.', 0, 7, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (47, 'Earum voluptatibus natus modi et sed quia aut qui.', 'Consequatur fugit optio illo labore. Dolorum modi debitis amet eos adipisci autem rem. Atque eum pariatur est totam eius. Aspernatur voluptatibus modi et.', 1, 10, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (48, 'Minus vero quas voluptas numquam eius et aperiam vero.', 'Sed rem sed eum quo ut dolor assumenda et. Est mollitia iusto voluptate ut neque velit. Error expedita expedita temporibus.', 0, 4, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (49, 'Aperiam veniam consequatur est.', 'Voluptates autem ab et autem necessitatibus inventore quam impedit. Sunt esse odit sint possimus eum. Debitis quae quia eum laudantium omnis cupiditate. Quaerat deserunt aliquam sequi dolorum impedit.', 0, 5, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (50, 'Qui quis porro dignissimos dolorum vel.', 'Facere aspernatur voluptas voluptatem nisi nam pariatur quam. Quia sint nam molestiae blanditiis. Qui corporis debitis esse est quia recusandae. Quam provident aspernatur rerum quos rerum id et.', 1, 4, '2021-10-13 17:37:46', '2021-10-13 17:37:46');
INSERT INTO `todos` VALUES (52, 'Judul 2 - update 52', 'body 2 - update 52', 1, 1, '2021-10-13 18:12:03', '2021-10-13 18:15:50');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'Jaime Bergnaum', 'shad20@example.com', '2021-10-13 17:35:44', '$2y$10$bUrRsjxW0MP5X5lMYvcFmeVE7FdqgXNdYCbV1mJI1iIvUsINs2pN2', '9uI5MRJvnm', '2021-10-13 17:35:44', '2021-10-13 17:35:44');
INSERT INTO `users` VALUES (2, 'Ruth Fritsch', 'sydni.dach@example.com', '2021-10-13 17:35:44', '$2y$10$AlH6BEhUCxTCfWsL3BnR7uGTmSoGmfgFFPzHDabRi26hvprmVH2rm', 'WuXxf2LlHm', '2021-10-13 17:35:44', '2021-10-13 17:35:44');
INSERT INTO `users` VALUES (3, 'Miss Lottie Cummings Jr.', 'borer.burdette@example.com', '2021-10-13 17:35:44', '$2y$10$J9f2wM3bQmB6rNdNzxeTmeOLjdfwkERLbMDVPatEo3EN9J91J07Q2', 'WxkUhM2cy8', '2021-10-13 17:35:44', '2021-10-13 17:35:44');
INSERT INTO `users` VALUES (4, 'Pearlie Hermiston', 'wilburn03@example.com', '2021-10-13 17:35:44', '$2y$10$wk/yh/T1TJlxFk.d6dXgveAVHLW85ALdKp7VAEh4DcwscyoFDvs9C', 'ALy0bhN2Va', '2021-10-13 17:35:44', '2021-10-13 17:35:44');
INSERT INTO `users` VALUES (5, 'Dakota Wunsch', 'filomena55@example.org', '2021-10-13 17:35:44', '$2y$10$RKljHlWxTi.zi9q0mwn7X.0HZnvUvybXvVcWFrsvYTjUU.sGjwTKS', '6jJ1Nkm2II', '2021-10-13 17:35:44', '2021-10-13 17:35:44');
INSERT INTO `users` VALUES (6, 'Kenny Stroman', 'prince02@example.com', '2021-10-13 17:35:44', '$2y$10$r4s6L4vhOzDa0h38CUT/Uu3F5oysAMIeIE0/dnPYOSGuqyTvPwf7.', 'VvfX8yiZDX', '2021-10-13 17:35:44', '2021-10-13 17:35:44');
INSERT INTO `users` VALUES (7, 'Ellie Greenfelder', 'tlueilwitz@example.net', '2021-10-13 17:35:44', '$2y$10$YJW5wP6ywDlsTpwvk.VLGuFngUORycDcIWS3BUxxWfwUOO75pW7oO', 'cME4NpkP83', '2021-10-13 17:35:44', '2021-10-13 17:35:44');
INSERT INTO `users` VALUES (8, 'Ms. Augusta Botsford', 'marcia.lehner@example.org', '2021-10-13 17:35:44', '$2y$10$av0qUTyOlG66HNOCvYdOh.ouXTmeu/PUow7jyc/DNcEoKwD5DsSAK', 'MLndpUMdrc', '2021-10-13 17:35:44', '2021-10-13 17:35:44');
INSERT INTO `users` VALUES (9, 'Alaina Ritchie', 'tillman.branson@example.net', '2021-10-13 17:35:44', '$2y$10$5b3GfnCuRSKbMgN7FjcbmuT4kQJzhJxgXPEfckKgo1LVh0bMRdrk.', '6BjO87dc7Q', '2021-10-13 17:35:44', '2021-10-13 17:35:44');
INSERT INTO `users` VALUES (10, 'Javon Bechtelar', 'daugherty.marta@example.com', '2021-10-13 17:35:44', '$2y$10$i2xb/CNSFxCtiS7ZC7T73eJUDvGNsqNn.pWU26wA7vxxTbPVFvN.6', 'v23ruCWtqa', '2021-10-13 17:35:44', '2021-10-13 17:35:44');
INSERT INTO `users` VALUES (11, 'tony arianto', 'tony@gmail.com', NULL, '$2y$10$CfKSXEYV9lxvpeNar8CDWO7KGv1LPVlylnP7Z.MoVDDDoP4w5Wo42', NULL, '2021-10-13 18:02:57', '2021-10-13 18:02:57');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
