<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\HrPHK;
use App\Models\OccuTree;
use App\Models\EmployeeDB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\ResponseBuilder;
use App\Http\Helper\ResponseBuilderList;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\HttpFoundation\Response;

class HrPHKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = HrPHK::all();
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eedb = EmployeeDB::where('nip', '=', $request->nip)->get()->toArray();

        $employee_status = $eedb['0']['employee_status'];

        if ($employee_status == 4) {

            $response = [
                'message'=>'Pegawai sudah berstatus bukan pegawai',
                'staus' => false,
            ];

            return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $validator = Validator::make($request->all(),[
            'nip' => ['required','exists:mysql_api.sys_ttrs_employee,nip'],  #'nip' => ['required','exists:mysql_api.sys_ttrs_employee,nip'],
            'apply_date_phk' => ['required'],
            'sys_tmst_alasan_phk_id' => ['required'],
            'not_active_work_date' => ['required']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $not_active_work_date = $request->not_active_work_date;
        $not_active_work_date = Carbon::parse($not_active_work_date);
        $not_active_work_date = $not_active_work_date->copy()->startOfDay();
        $now = Carbon::parse(now());
        $now = $now->copy()->startOfDay();
        $date_diff = $now->diffInDays($not_active_work_date, false);

        if($date_diff >= 0) {
            $data = [];
            $data['id'] = Str::orderedUuid()->getHex();
            $data['tmpid'] = $request-> tmpid;
            $data['sys_tmst_company_id'] = '1';
            $data['sys_ttrs_employee_id'] = $eedb['0']['id'];
            $data['no_sk'] = $request-> no_sk;
            $data['sys_tmst_alasan_phk_id'] =  $request-> sys_tmst_alasan_phk_id;
            $data['apply_date_phk'] = $request-> apply_date_phk;
            $data['not_active_work_date'] = $request-> not_active_work_date;
            $data['last_employee_status'] = $eedb['0']['employee_status'];
            $data['description'] = $request-> description;
            $data['fmode'] = '4';
            $data['fstatus'] = '0';
            $data['createdate'] = now();
            $data['src_origin'] = 1;
            $data['moduser'] = '2';
            $data['moddate'] = now();

            $insert = HrPHK::insert($data);
            //(Boolean)$insert = true;

            $response = [
                'message'   =>'Data successfully inserted.',
                'status'    => $insert,
                'data'      => $data
            ];

            return response()->json($response, Response::HTTP_CREATED);

        } else {
            $data = [];
            $data['id'] = Str::orderedUuid()->getHex();
            $data['tmpid'] = $request-> tmpid;
            $data['sys_tmst_company_id'] = '1';
            $data['sys_ttrs_employee_id'] = $eedb['0']['id'];
            $data['no_sk'] = $request-> no_sk;
            $data['sys_tmst_alasan_phk_id'] =  $request-> sys_tmst_alasan_phk_id;
            $data['apply_date_phk'] = $request-> apply_date_phk;
            $data['not_active_work_date'] = $request-> not_active_work_date;
            $data['last_employee_status'] = $eedb['0']['employee_status'];
            $data['description'] = $request-> description;
            $data['fmode'] = '4';
            $data['fstatus'] = '0';
            $data['createdate'] = now();
            $data['src_origin'] = 1;
            $data['moduser'] = '2';
            $data['moddate'] = now();

            $insert = $insert = HrPHK::insert($data);

            if($insert == true) {

                $update_ee = [];
                $update_ee['last_employee_status'] = $eedb['0']['employee_status'];
                $update_ee['employee_status'] = 4;
                $update_ee['sys_tmst_employee_type_id'] = 11;
                $update_ee['resign_date'] = $request-> not_active_work_date;
                $update_ee['resign_reason'] = $request-> sys_tmst_alasan_phk_id;
                $update_ee['moddate'] = now();

                //dd($update_ee);

                $update_occutree = [];
                $update_occutree['sys_ttrs_employee_id'] = null;
                $update_occutree['src_origin'] = 1;
                $update_occutree['moddate'] = now();

                //dd($update_occutree);

                $update_employeedb = EmployeeDB::where('id', '=', $eedb['0']['id'])
                                    ->update($update_ee);

                $update_occu_tree = OccuTree::where('sys_ttrs_employee_id', '=', $eedb['0']['id'])
                                    ->update($update_occutree);

            }

            $update_employeedb = ($update_employeedb = 1 ? true : false);
            $update_occu_tree = ($update_occu_tree = 1 ? true : false);

            $data_status = [
                'message'=>'Data successfully inserted.',
                'status_insert_phk'=> $insert,
                'status_update_employee' => $update_employeedb,
                'status_update_occutree' => $update_occu_tree,
                'data_insert_phk'=> $data,
                'data_update_employee'=> $update_ee,
                'data_update_occutree'=> $update_occutree
            ];
            return response()->json($data_status, Response::HTTP_CREATED);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HrPHKController  $hrPHKController
     * @return \Illuminate\Http\Response
     */
    public function show(HrPHK $hrPHK)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HrPHKController  $hrPHKController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $nip)
    {

        $eedb = EmployeeDB::where('nip','=', $nip)->get()->toArray();

        $employee_status = $eedb['0']['employee_status'];

        if ($employee_status == 4) {

            $response = [
                'message'=>'Pegawai sudah berstatus mantan pegawai',
                'staus' => false,
            ];

            return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $validator = Validator::make($request->all(),[
            // 'nip' => ['required','exists:mysql_api.sys_ttrs_employee,nip'],  #'nip' => ['required','exists:mysql_api.sys_ttrs_employee,nip'],
            'apply_date_phk' => ['required'],
            'sys_tmst_alasan_phk_id' => ['required'],
            'not_active_work_date' => ['required']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $data = [];
        $data['no_sk'] = $request -> no_sk;
        // $data['nip'] = $nip;
        $data['sys_tmst_alasan_phk_id'] = $request -> sys_tmst_alasan_phk_id;
        $data['apply_date_phk'] = $request -> apply_date_phk;
        $data['not_active_work_date'] = $request -> not_active_work_date;
        $data['description'] = $request -> description;
        $data['moddate'] = now();


        $update = HrPHK::where('sys_ttrs_employee_id','=', $eedb['0']['id'] )
                ->update($data);

        $status_update = ($update == 1 ? true : false);

        $response = [
            'message'=>'Data berhasil diupdate.',
            'status'=> $status_update,
            'data' => $data
        ];

        return response()->json($response, Response::HTTP_CREATED);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HrPHKController  $hrPHKController
     * @return \Illuminate\Http\Response
     */
    public function destroy(HrPHK $hrPHK)
    {
        //
    }
}
