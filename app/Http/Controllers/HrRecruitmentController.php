<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\OccuTree;
use App\Models\EmployeeDB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\HrRecruitment;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Database\QueryException;
use App\Http\Helper\ResponseBuilderList;
use App\Models\BankAccount;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class HrRecruitmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
   {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = HrRecruitment::all();
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        #--------
        // 1. cek idjabatan kosong atau tidak | harus kosong
        // 2. Cek tanggal startwork | jika kurang dari atau samadengan hari ini :
        //          posting ke 1. sys_ttrs_employee_recruitment
        //          update ke 2. sys_ttrs_employee
        //          update ke 3. sys_tmst_occupation_tree
        // 3. jika kurang lebih dari hari ini
        //          posting ke 1. sys_ttrs_employee_recruitment
        // 4. tmpid tidak bisa di posting 2 kali
        // untuk testing, clear => tmpid > di sys_ttrs_employee_recruitment, clear => sys_ttrs_employee_id di occupation
        //                dan perhatikan di startworknya.

        #--------
        // ambil referensi ke table employee
        //dd($request->all());

        $eedb = EmployeeDB::where('tmpid','=', $request->tmpid)->get()->toArray();
        $max_idhrpay = (BankAccount::max('id')) + 1;

        if ($eedb == null) {

            $response = [
                'message'=>'tmpid yang anda pilih tidak ditemukan.',
                'staus' => false,
            ];

            return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // $occutree = OccuTree::where('sys_tmst_occupation_id','=', $request->sys_tmst_occupation_id)->get()->toArray();

        // $test = $occutree['0']['sys_ttrs_employee_id'];

        // dd($test);

        //pengecekan jabatan harus kosong
        $occu_tree = OccuTree::where('sys_tmst_occupation_id','=', $request->sys_tmst_occupation_id)->get()->toArray();

        // dd($occu_tree);

        $id_occu_tree = $occu_tree['0']['sys_ttrs_employee_id'];

        $id_occu_tree = (($id_occu_tree == null || $id_occu_tree == 0) ? true : false);

        //dd($id_occu_tree);

        if ($id_occu_tree == false) {

            $response = [
                'message'=>'Jabatan yang anda pilih sudah terisi.',
                'staus' => false,
            ];

            return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        } else {
            $validator = Validator::make($request->all(),[
                'tmpid' => ['required'], #unique:tenant.clients
                'nip' => ['required','unique:mysql_api.sys_ttrs_employee,nip'],
                'startwork' => ['required'],
                'tax_month' => ['required'],
                'sys_tmst_company_id' => ['required','numeric','min:1','max:9'],
                'sys_tmst_level_id' => ['required','numeric','min:1'],
                'sys_tmst_unit_id' => ['required','numeric','min:1','max:8'],
                'sys_tmst_grade_id' => ['required','numeric','min:1','max:8'],
                'sys_tmst_department_id' => ['required','numeric','min:1'],
                'sys_tmst_department_group_divisi_id' => ['required','numeric','min:1','max:8'],
                'sys_tmst_occupation_id' => ['required','min:1','max:32'],
                'sys_tmst_cost_center_id' => ['required','min:1','max:32'],
                'jkk_code' => ['required','min:2','max:32'],
                'custom1' => ['required','min:1','max:32'],
                'custom2' => ['required','min:1','max:32'],
                'custom3' => ['required','min:1','max:32'],
                'custom7' => ['required','min:1','max:32'],
                'bank_account.pay_method' => ['required','min:1','max:32'],
                'bank_account.account_type' => ['required','min:1','max:32'],
                'bank_account.account_name' => ['required','min:1','max:64'],
                'bank_account.account_no' => ['required','min:1','max:32'],
                'bank_account.sys_tmst_bank_id' => ['required','min:1','max:32']
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            //pengecekan startwork >= hari ini ?

            $startwork = $request->startwork;
            $startwork = Carbon::parse($startwork);
            $startwork = $startwork->copy()->startOfDay();

            $now = Carbon::parse(now());
            $now = $now->copy()->startOfDay();

            $date_diff = $now->diffInDays($startwork, false);

            if($date_diff >= 1) {
                $data = array();
                $data['id'] = Str::orderedUuid()->getHex();
                //$data['tmpid'] = Str::orderedUuid()->getHex();
                $data['tmpid'] = $request->tmpid;
                $data['sys_ttrs_employee_id'] = $eedb['0']['id'];
                $data['nip'] = $request->nip;
                $data['tax_npwp_personal'] = $request->tax_npwp_personal;
                $data['startdate'] = $request->startdate;
                $data['enddate'] = $request->enddate;
                $data['extenddate'] = $request->extenddate;
                $data['startwork'] = $request->startwork;
                $data['tax_month'] = $request->tax_month;
                $data['no_sk'] = $request->no_sk;
                $data['employee_status'] = $request->fmode;
                $data['last_employee_status'] = $eedb['0']['employee_status'];
                $data['sys_tmst_kpp_id'] = $request->sys_tmst_kpp_id;
                $data['sys_tmst_company_id'] = $request->sys_tmst_company_id;
                $data['sys_tmst_level_id'] = $request->sys_tmst_level_id;
                $data['sys_tmst_unit_id'] = $request->sys_tmst_unit_id;
                $data['sys_tmst_grade_id'] = $request->sys_tmst_grade_id;
                $data['sys_tmst_department_id'] = $request->sys_tmst_department_id;
                $data['sys_tmst_department_group_divisi_id'] = $request->sys_tmst_department_group_divisi_id;
                $data['sys_tmst_occupation_id'] = $request->sys_tmst_occupation_id;
                $data['sys_tmst_occupation_tree_id'] = $occu_tree['0']['id'];
                $data['sys_tmst_timeshift_id'] = $request->sys_tmst_timeshift_id;
                $data['journal_code'] = $request->journal_code;
                $data['sys_tmst_company_npwp_id'] = $request->sys_tmst_company_npwp_id;
                $data['sys_tmst_company_npp_id'] = $request->sys_tmst_company_npp_id;
                $data['sys_tmst_cost_center_id'] = $request->sys_tmst_cost_center_id;
                $data['username_employee'] = $request->username_employee;
                $data['mail_office'] = $request->mail_office;
                $data['absen_id'] = $request->absen_id;
                $data['sys_tmst_overtime_profile_id'] = $request->sys_tmst_overtime_profile_id;
                $data['pph_resume'] = $request->pph_resume;
                $data['pph_pajak'] = $request->pph_pajak;
                $data['jkk_code'] = $request->jkk_code;
                $data['jkk_value'] = $request->jkk_value;
                $data['bpjs_num'] = $request->bpjs_num;
                $data['bpjs_num_others'] = $request->bpjs_num_others;
                $data['marital_status_tax'] = $request->marital_status_tax;
                $data['fmode'] = $request->fmode;
                $data['history_status'] = $request->history_status;
                $data['main_opttax'] = $request->main_opttax;
                $data['jamsostek_code'] = $request->jamsostek_code;
                $data['fstatus'] = $request->fstatus;
                $data['sys_tmst_attendance_shift_id'] = $request->sys_tmst_attendance_shift_id;
                $data['custom1'] = $request->custom1;
                $data['custom2'] = $request->custom2;
                $data['custom3'] = $request->custom3;
                $data['custom4'] = $request->custom4;
                $data['custom7'] = $request->custom7;
                $data['description'] = $request->description;
                $data['src_origin'] = 1;
                $data['createdate'] = now();
                $data['moduser'] = '2';
                $data['moddate'] = now();

                $hrpay = [];
                $hrpay['id'] = $max_idhrpay;
                $hrpay['sys_ttrs_employee_id'] = $eedb['0']['id'];
                $hrpay['ftype'] = 2;
                $hrpay['pay_method'] = 1;
                $hrpay['account_name'] = $request['bank_account']['account_name'];
                $hrpay['account_no'] = $request['bank_account']['account_no'];
                $hrpay['sys_tmst_bank_id'] = $request['bank_account']['sys_tmst_bank_id'];
                $hrpay['branch'] = $request['bank_account']['branch'];
                $hrpay['account_type'] = 1;
                $hrpay['status'] = null;
                $hrpay['attribute'] = null;
                $hrpay['description'] = null;
                $hrpay['createdate'] = now();
                $hrpay['moduser'] = 2;
                $hrpay['moddate'] = now();


                //$insert = DB::table('sys_ttrs_employee_recruitment')->insert($data);
                $insert_hr = HrRecruitment::insert($data);
                $insert_hrpay = BankAccount::insert($hrpay);

                $response = [
                    'message'=>'Data successfully inserted.',
                    'status_insert'=> $insert_hr,
                    'insert_hrpay'=> $insert_hrpay,
                    'data_insert_hr' => $data,
                    'data_insert_hrpay' => $data
                ];

                return response()->json($response, Response::HTTP_CREATED);

            } else {
                //jika kurang dari hari ini
                $data = array();
                $data['id'] = Str::orderedUuid()->getHex();
                $data['tmpid'] = $request->tmpid;
                $data['sys_ttrs_employee_id'] = $eedb['0']['id'];;
                $data['nip'] = $request->nip;
                $data['tax_npwp_personal'] = $request->tax_npwp_personal;
                $data['startdate'] = $request->startdate;
                $data['enddate'] = $request->enddate;
                $data['extenddate'] = $request->extenddate;
                $data['startwork'] = $request->startwork;
                $data['tax_month'] = $request->tax_month;
                $data['no_sk'] = $request->no_sk;
                $data['employee_status'] = $request->fmode;
                $data['last_employee_status'] = $eedb['0']['employee_status'];
                $data['sys_tmst_kpp_id'] = $request->sys_tmst_kpp_id;
                $data['sys_tmst_company_id'] = $request->sys_tmst_company_id;
                $data['sys_tmst_level_id'] = $request->sys_tmst_level_id;
                $data['sys_tmst_unit_id'] = $request->sys_tmst_unit_id;
                $data['sys_tmst_grade_id'] = $request->sys_tmst_grade_id;
                $data['sys_tmst_department_id'] = $request->sys_tmst_department_id;
                $data['sys_tmst_department_group_divisi_id'] = $request->sys_tmst_department_group_divisi_id;
                $data['sys_tmst_occupation_id'] = $request->sys_tmst_occupation_id;
                $data['sys_tmst_occupation_tree_id'] = $occu_tree['0']['id'];
                $data['sys_tmst_timeshift_id'] = $request->sys_tmst_timeshift_id;
                $data['journal_code'] = $request->journal_code;
                $data['sys_tmst_company_npwp_id'] = $request->sys_tmst_company_npwp_id;
                $data['sys_tmst_company_npp_id'] = $request->sys_tmst_company_npp_id;
                $data['sys_tmst_cost_center_id'] = $request->sys_tmst_cost_center_id;
                $data['username_employee'] = $request->username_employee;
                $data['mail_office'] = $request->mail_office;
                $data['absen_id'] = $request->absen_id;
                $data['sys_tmst_overtime_profile_id'] = $request->sys_tmst_overtime_profile_id;
                $data['pph_resume'] = $request->pph_resume;
                $data['pph_pajak'] = $request->pph_pajak;
                $data['jkk_code'] = $request->jkk_code;
                $data['jkk_value'] = $request->jkk_value;
                $data['bpjs_num'] = $request->bpjs_num;
                $data['bpjs_num_others'] = $request->bpjs_num_others;
                $data['marital_status_tax'] = $request->marital_status_tax;
                $data['fmode'] = $request->fmode;
                $data['history_status'] = $request->history_status;
                $data['main_opttax'] = $request->main_opttax;
                $data['jamsostek_code'] = $request->jamsostek_code;
                $data['fstatus'] = $request->fstatus;
                $data['sys_tmst_attendance_shift_id'] = $request->sys_tmst_attendance_shift_id;
                $data['custom1'] = $request->custom1;
                $data['custom2'] = $request->custom2;
                $data['custom3'] = $request->custom3;
                $data['custom4'] = $request->custom4;
                $data['custom7'] = $request->custom7;
                $data['description'] = $request->description;
                $data['src_origin'] = 1;
                $data['createdate'] = now();
                $data['moduser'] = '2';
                $data['moddate'] = now();

                $insert = HrRecruitment::insert($data);

                if ($insert == true) {
                    //jika berhasil innert ke sys_ttrs_employee_recruitment
                    $update_tree = [];
                    $update_tree['sys_ttrs_employee_id'] = $eedb['0']['id'];;
                    $update_tree['moddate'] = now();

                    $data_update_ee = array();
                    $data_update_ee['nip'] = $request->nip;
                    $data_update_ee['tax_npwp_personal'] = $request->tax_npwp_personal;
                    $data_update_ee['startdate'] = $request->startdate;
                    $data_update_ee['startwork'] = $request->startwork;
                    $data_update_ee['tax_month'] = $request->tax_month;
                    $data_update_ee['employee_status'] = $request->fmode;
                    $data_update_ee['last_employee_status'] = $eedb['0']['employee_status'];
                    $data_update_ee['sys_tmst_kpp_id'] = $request->sys_tmst_kpp_id;
                    $data_update_ee['sys_tmst_company_id'] = $request->sys_tmst_company_id;
                    $data_update_ee['sys_tmst_level_id'] = $request->sys_tmst_level_id;
                    $data_update_ee['sys_tmst_unit_id'] = $request->sys_tmst_unit_id;
                    $data_update_ee['sys_tmst_grade_id'] = $request->sys_tmst_grade_id;
                    $data_update_ee['sys_tmst_department_id'] = $request->sys_tmst_department_id;
                    $data_update_ee['sys_tmst_department_group_divisi_id'] = $request->sys_tmst_department_group_divisi_id;
                    $data_update_ee['sys_tmst_occupation_id'] = $request->sys_tmst_occupation_id;
                    $data_update_ee['journal_code'] = $request->journal_code;
                    $data_update_ee['sys_tmst_company_npwp_id'] = $request->sys_tmst_company_npwp_id;
                    $data_update_ee['sys_tmst_company_npp_id'] = $request->sys_tmst_company_npp_id;
                    $data_update_ee['sys_tmst_cost_center_id'] = $request->sys_tmst_cost_center_id;
                    $data_update_ee['username'] = $request->username_employee;
                    $data_update_ee['mail_office'] = $request->mail_office;
                    $data_update_ee['absen_id'] = $request->absen_id;
                    $data_update_ee['jkk_code'] = $request->jkk_code;
                    $data_update_ee['jkk_value'] = $request->jkk_value;
                    $data_update_ee['bpjs_num'] = $request->bpjs_num;
                    $data_update_ee['bpjs_num_others'] = $request->bpjs_num_others;
                    $data_update_ee['marital_status_tax'] = $request->marital_status_tax;
                    $data_update_ee['main_opttax'] = $request->main_opttax;
                    $data_update_ee['jamsostek_code'] = $request->jamsostek_code;
                    $data_update_ee['sys_tmst_attendance_shift_id'] = $request->sys_tmst_attendance_shift_id;
                    $data_update_ee['custom1'] = $request->custom1;
                    $data_update_ee['custom2'] = $request->custom2;
                    $data_update_ee['custom3'] = $request->custom3;
                    $data_update_ee['custom4'] = $request->custom4;
                    $data_update_ee['custom7'] = $request->custom7;
                    //$data_update_ee['src_origin'] = 1;
                    $data_update_ee['moddate'] = now();

                    $hrpay = [];
                    $hrpay['id'] = $max_idhrpay;
                    $hrpay['sys_ttrs_employee_id'] = $eedb['0']['id'];
                    $hrpay['ftype'] = 2;
                    $hrpay['pay_method'] = 1;
                    $hrpay['account_name'] = $request['bank_account']['account_name'];
                    $hrpay['account_no'] = $request['bank_account']['account_no'];
                    $hrpay['sys_tmst_bank_id'] = $request['bank_account']['sys_tmst_bank_id'];
                    $hrpay['branch'] = $request['bank_account']['branch'];
                    $hrpay['account_type'] = 1;
                    $hrpay['status'] = null;
                    $hrpay['attribute'] = null;
                    $hrpay['description'] = null;
                    $hrpay['createdate'] = now();
                    $hrpay['moduser'] = 2;
                    $hrpay['moddate'] = now();

                    // $update_ee = DB::table('sys_ttrs_employee') # metode ini akan mengambil data dari default connection
                    //                 ->where('nip','=', $request->nip)
                    //                 ->update($data_update_ee);
                    $update_ee = EmployeeDB::where('tmpid','=', $request->tmpid)
                                            ->update($data_update_ee);

                    $insert_hrpay = BankAccount::insert($hrpay);


                    if ($id_occu_tree == true) {

                        // $update_occu_tree = DB::table('sys_tmst_occupation_tree')
                        //             ->where('sys_tmst_occupation_id', '=', $request->sys_tmst_occupation_id)
                        //             ->update($update_tree);

                        $update_occu_tree = OccuTree::where('sys_tmst_occupation_id','=', $request->sys_tmst_occupation_id)
                                                    ->update($update_tree);

                    }
                }

                $update_ee = ($update_ee = 1 ? true : false);
                $update_occu_tree = ($update_occu_tree = 1 ? true : false);
                $insert_hrpay = ($insert_hrpay = 1 ? true : false);

                // cek status inert dan update
                $data_status = [
                    'message'=>'Data successfully inserted.',
                    'status_insert_recruitment'=> $insert,
                    'status_insert_hrpay'=> $insert_hrpay,
                    'status_update_employee' => $update_ee,
                    'status_update_occu_tree' => $update_occu_tree,
                    'insert_recruitment' => $data,
                    'insert_hrpay' => $hrpay,
                    'update_employee' => $data_update_ee,
                    'update_occutree' => $update_tree,
                ];

                return response()->json($data_status, Response::HTTP_CREATED);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
