<?php

namespace App\Http\Controllers;

use App\Models\EmployeeDB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\ResponseBuilder;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\QueryException;
use App\Http\Helper\ResponseBuilderList;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;


class EmployeeDBController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /*
        $employee = EmployeeDB::all();
        return response()->json([
            'pesan' => 'Data berhasil di ambil',
            'status' => 'sukses',
            'data' => $employee
        ]);
        */

        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        // $data = EmployeeDB::where('nip', 'ID011407441')->first();
        $data = EmployeeDB::paginate(15);

        return ResponseBuilder::result($status, $message, $data, $response_code);


    }

    public function all()
    {

        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        // $data = EmployeeDB::where('nip', 'ID011407441')->first();
        $data = EmployeeDB::all();
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);


    }

    public function update(Request $request, $nip)
    {
        //DB::connection('db_test');

        EmployeeDB::where('nip', '=', $nip)->firstOrFail();

        $validator = Validator::make($request->all(),[
            'nip'=> ['required'],
            'sys_tmst_department_group_divisi_id'=> ['required'],
            'sys_tmst_company_npwp_id'=> ['required'],
            'sys_tmst_company_npp_id'=> ['required'],
            'sys_tmst_cost_center_id'=> ['required'],
            'firstname'=> ['required'],
            'sex'=> ['required'],
            'sys_tmst_marital_status_id'=> ['required'],
            'sys_tmst_religion_id'=> ['required'],
            'sys_tmst_country_id'=> ['required'],
            'sys_tmst_identity_type_id'=> ['required'],
            'identity_number'=> ['required'],
            'lv_sys_tmst_country_id'=> ['required'],
            'lv_sys_tmst_province_id'=> ['required'],
            'lv_sys_tmst_regency_id'=> ['required'],
            'lv_sys_tmst_district_id'=> ['required'],
            'lv_kelurahan'=> ['required'],
            'lv_rt'=> ['required'],
            'lv_rw'=> ['required'],
            'lv_address'=> ['required'],
            'id_sys_tmst_province_id'=> ['required'],
            'id_sys_tmst_regency_id'=> ['required'],
            'id_sys_tmst_district_id'=> ['required'],
            'id_kelurahan'=> ['required'],
            'id_rt'=> ['required'],
            'id_rw'=> ['required'],
            'id_address'=> ['required'],
            'jamsostek_code'=> ['required'],
            'jkk_code'=> ['required'],
            'bpjs_num'=> ['required'],
            'kk_num'=> ['required'],
            'custom1'=> ['required'],
            'custom2'=> ['required'],
            'custom3'=> ['required'],
            'custom7'=> ['required'],
            'custom9'=> ['required'],
            'custom10'=> ['required'],
            'custom11'=> ['required'],
            'custom13'=> ['required']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }


        $data = array();
        $data['absen_id'] = $request-> absen_id;
        $data['nip'] = $request-> nip;
        $data['sys_ttrs_employee_group_id'] = $request-> sys_ttrs_employee_group_id;
        $data['sys_tmst_company_id'] = 1;
        // $data['sys_tmst_company_id'] = $request-> sys_tmst_company_id;
        $data['sys_tmst_level_id'] = $request-> sys_tmst_level_id;
        $data['sys_tmst_unit_id'] = $request-> sys_tmst_unit_id;
        $data['sys_tmst_grade_id'] = $request-> sys_tmst_grade_id;
        $data['sys_tmst_department_id'] = $request-> sys_tmst_department_id;
        $data['sys_tmst_department_group_divisi_id'] = $request-> sys_tmst_department_group_divisi_id;
        $data['sys_tmst_occupation_id'] = $request-> sys_tmst_occupation_id;
        $data['sys_tmst_cost_center_id'] = $request-> sys_tmst_cost_center_id;
        $data['sys_tmst_company_npwp_id'] = $request -> sys_tmst_company_npwp_id;
        $data['sys_tmst_company_npp_id'] = $request -> sys_tmst_company_npp_id;
        $data['jkk_code'] = $request-> jkk_code;
        $data['firstname'] = $request-> firstname;
        $data['lastname'] = $request-> lastname;
        $data['surname'] = $request-> surname;
        $data['username'] = $request-> username;
        $data['placeofborn'] = $request-> placeofborn;
        $data['dateofborn'] = $request-> dateofborn;
        $data['sex'] = $request-> sex;
        $data['sys_tmst_marital_status_id'] = $request-> sys_tmst_marital_status_id;
        $data['marital_status_tax'] = $request-> marital_status_tax;
        $data['couple_name'] = $request-> couple_name;
        $data['sys_tmst_religion_id'] = $request-> sys_tmst_religion_id;
        $data['sys_tmst_country_id'] = $request-> sys_tmst_country_id;
        $data['startincountry'] = $request-> startincountry;
        $data['sys_tmst_identity_type_id'] = $request-> sys_tmst_identity_type_id;
        $data['identity_number'] = $request-> identity_number;
        $data['mother_name'] = $request-> mother_name;
        $data['married_date'] = $request-> married_date;
        $data['sys_tmst_blood_type_id'] = $request-> sys_tmst_blood_type_id;
        $data['tall_body'] = $request-> tall_body;
        $data['weight_body'] = $request-> weight_body;
        $data['recruitment_origin'] = $request-> recruitment_origin;
        $data['lv_sys_tmst_country_id'] = $request-> lv_sys_tmst_country_id;
        $data['lv_sys_tmst_province_id'] = $request-> lv_sys_tmst_province_id;
        $data['lv_sys_tmst_regency_id'] = $request-> lv_sys_tmst_regency_id;
        $data['lv_sys_tmst_district_id'] = $request-> lv_sys_tmst_district_id;
        $data['lv_kelurahan'] = $request-> lv_kelurahan;
        $data['lv_rt'] = $request-> lv_rt;
        $data['lv_rw'] = $request-> lv_rw;
        $data['lv_address'] = $request-> lv_address;
        $data['lv_postcode'] = $request-> lv_postcode;
        $data['id_sys_tmst_province_id'] = $request-> id_sys_tmst_province_id;
        $data['id_sys_tmst_regency_id'] = $request-> id_sys_tmst_regency_id;
        $data['id_sys_tmst_district_id'] = $request-> id_sys_tmst_district_id;
        $data['id_kelurahan'] = $request-> id_kelurahan;
        $data['id_rt'] = $request-> id_rt;
        $data['id_rw'] = $request-> id_rw;
        $data['id_address'] = $request-> id_address;
        $data['id_postcode'] = $request-> id_postcode;
        $data['homephone'] = $request-> homephone;
        $data['officephone'] = $request-> officephone;
        $data['mobile1'] = $request-> mobile1;
        $data['mobile2'] = $request-> mobile2;
        $data['skype'] = $request-> skype;
        $data['mail_office'] = $request-> mail_office;
        $data['mail_personal'] = $request-> mail_personal;
        $data['fb'] = $request-> fb;
        $data['ym'] = $request-> ym;
        $data['whatsup'] = $request-> whatsup;
        $data['twitter'] = $request-> twitter;
        $data['blog'] = $request-> blog;
        $data['hangout'] = $request-> hangout;
        $data['hobby'] = $request-> hobby;
        $data['tax_npwp_personal'] = $request-> tax_npwp_personal;
        $data['tax_npwp_couple'] = $request-> tax_npwp_couple;
        $data['status_tax'] = $request-> status_tax;
        $data['tax_treaty'] = $request-> tax_treaty;
        $data['jobcode'] = $request-> jobcode;
        $data['tid'] = $request-> tid;
        $data['employee_status'] = $request-> employee_status;
        $data['status'] = $request-> status;
        $data['nkj'] = $request-> nkj;
        $data['kk_num'] = $request-> kk_num;
        $data['passport_num'] = $request-> passport_num;
        $data['passport_expired'] = $request-> passport_expired;
        $data['tax_emp_percent'] = $request-> tax_emp_percent;
        $data['tax_comp_percent'] = $request-> tax_comp_percent;
        $data['custom1'] = $request-> custom1;
        $data['custom2'] = $request-> custom2;
        $data['custom3'] = $request-> custom3;
        $data['custom7'] = $request-> custom7;
        $data['custom4'] = $request-> custom4;
        $data['custom9'] = $request-> custom9;
        $data['custom10'] = $request-> custom10;
        $data['custom11'] = $request-> custom11;
        $data['custom12'] = $request-> custom12;
        $data['custom13'] = $request-> custom13;
        $data['custom14'] = $request-> custom14;
        $data['custom15'] = $request-> custom15;
        $data['moddate'] = now();

        $insert = EmployeeDB::where('nip','=', $request->nip)
                            ->update($data);
        //$insert = DB::table('sys_ttrs_employee')->update($data);

        $status_insert = ($insert == 1 ? true : false);

        $response = [
            'message'=>'Data berhasil diupdate.',
            'status'=> $status_insert,
            'data' => $data
        ];

        return response()->json($response, Response::HTTP_CREATED);

    }


    public function search() {

        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;

        $data = QueryBuilder::for(EmployeeDB::class)
        ->allowedFilters(['nip','firstname'])
        ->get();

        if (empty($data)){
            $message  = "Data kosong";
            return ResponseBuilder::result('False', $message, '[]', '404');
        }

        return ResponseBuilder::result($status, $message, $data, $response_code);

    }

    public function show($nip)
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        //$data = EmployeeDB::find($nip);

        $data = EmployeeDB::where('nip', $nip)->get();

        try {
            if (empty($data)){
                    $message  = "Data kosong";
                    return ResponseBuilder::result('False', $message, '[]', '404');
                }
            return ResponseBuilder::result($status, $message, $data, $response_code);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }



}
