<?php

namespace App\Http\Controllers;

use App\Models\EmployeeDB;
use App\Models\BankAccount;
use Illuminate\Http\Request;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Database\QueryException;
use App\Http\Helper\ResponseBuilderList;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class BankAccountController extends Controller
{
    public function index () {
        $data = BankAccount::from('sys_ttrs_employee_hr_pay as p')
            ->selectRaw('p.id,
                         e.nip,
                         p.pay_method,
                         p.account_name,
                         p.account_no,
                         p.sys_tmst_bank_id,
                         p.branch
                        ')
            ->join('sys_ttrs_employee as e', 'e.id', '=', 'p.sys_ttrs_employee_id')
            ->where('e.employee_status','!=',4)
            ->get();

        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);
    }

    public function store (Request $request) {

        $validator = Validator::make($request->all(),[
            'nip' => ['required'],
            'account_name' => ['required','min:1','max:64'],
            'account_no' => ['required','min:1','max:32'],
            'sys_tmst_bank_id' => ['required','min:1','max:16']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $eedb = EmployeeDB::where('nip','=', $request->nip)->get()->toArray();
        $max_idhrpay = (BankAccount::max('id')) + 1;

        $hrpay = [];
        $hrpay['id'] = $max_idhrpay;
        $hrpay['sys_ttrs_employee_id'] = $eedb['0']['id'];
        $hrpay['ftype'] = 2;
        $hrpay['pay_method'] = 1;
        $hrpay['account_name'] = $request->account_name;
        $hrpay['account_no'] = $request->account_no;
        $hrpay['sys_tmst_bank_id'] = $request->sys_tmst_bank_id;
        $hrpay['branch'] = $request->branch;
        $hrpay['account_type'] = 1;
        $hrpay['status'] = null;
        $hrpay['attribute'] = null;
        $hrpay['description'] = null;
        $hrpay['createdate'] = now();
        $hrpay['moduser'] = 2;
        $hrpay['moddate'] = now();

        $insert_hrpay = BankAccount::insert($hrpay);

        $response = [
            'message'=>'Data successfully inserted.',
            'status_insert_hrpay'=> $insert_hrpay,
            'data_insert_hrpay' => $hrpay
        ];

        return response()->json($response, Response::HTTP_CREATED);

    }

    public function update(Request $request, $id)
    {
        $status = true;
        $message  = "Data successfully update.";
        $response_code = Response::HTTP_CREATED;
        $data = BankAccount::find($id);

        if (empty($data)){
                $message  = "ID tidak ditemukan";
                return ResponseBuilder::result('False', $message, '[]', '404');
        }

        $hrpay = [];
        $hrpay['ftype'] = 2;
        $hrpay['pay_method'] = 1;
        $hrpay['account_name'] = $request->account_name;
        $hrpay['account_no'] = $request->account_no;
        $hrpay['sys_tmst_bank_id'] = $request->sys_tmst_bank_id;
        $hrpay['branch'] = $request->branch;
        $hrpay['account_type'] = 1;
        $hrpay['moddate'] = now();

        $update = BankAccount::where('id','=', $id)
                ->update($hrpay);

        return ResponseBuilder::result($status, $message, $hrpay, $response_code);

    }

    public function destroy($id)
    {
        $status = true;
        $message  = "Data berhasil di hapus";
        $response_code = Response::HTTP_OK;
        $data = BankAccount::find($id);

        if (empty($data)){
                $message  = "ID tidak ditemukan";
                return ResponseBuilder::result('False', $message, '[]', '404');
            }

        $data->delete();

        return ResponseBuilder::result($status, $message, $data, $response_code);

    }
}
