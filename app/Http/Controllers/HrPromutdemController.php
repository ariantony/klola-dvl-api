<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\HrPromutdem;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Database\QueryException;
use App\Http\Helper\ResponseBuilderList;
use App\Models\EmployeeDB;
use App\Models\OccuTree;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class HrPromutdemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = HrPromutdem::all();
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //fmode = 5 => Mutasi, 6 =>Promosi, 7 =>Demosi, 8 => Rotasi, 9 => Perpanjangan Kontrak
        //cari sys_id

        $eedb = EmployeeDB::where('nip','=', $request->nip )->get()->toArray();

        if ($eedb == null) {

            $response = [
                'message'=>'NIP yang anda pilih tidak ditemukan.',
                'staus' => false,
            ];

            return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $sys_id = $eedb['0']['id'];

        //cek jabatan kosong

        $occu_tree = OccuTree::where('sys_tmst_occupation_id','=', $request->sys_tmst_occupation_id)->get()->toArray();
        $id_occu_tree = $occu_tree['0']['sys_ttrs_employee_id'];


        $id_occu_tree = (($id_occu_tree == null || $id_occu_tree == 0) ? true : false);


        if ($id_occu_tree == false) {

            $response = [
                'message'=>'Jabatan yang anda pilih sudah terisi.',
                'staus' => false,
            ];

            return response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        //jika memilih perpanjangan kontrak (fmode=9)
        if ($request->fmode == 9) {
            $validator = Validator::make($request->all(),[
                'nip' => ['required'],
                'tmpid' => ['required','unique:sys_ttrs_promutdem,tmpid'],
                'startdate' => ['required'],
                'sys_tmst_level_id' => ['required','min:1'],
                'sys_tmst_unit_id' => ['required','min:1'],
                'fmode' => ['required','min:1'],
                'enddate' => ['required','min:1']
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
            }

        } else {
            //selain fmode=9
            $validator = Validator::make($request->all(),[
                'nip' => ['required'],
                'tmpid' => ['required','unique:sys_ttrs_promutdem,tmpid'],
                'startdate' => ['required'],
                'sys_tmst_level_id' => ['required','min:1'],
                'sys_tmst_unit_id' => ['required','min:1'],
                'fmode' => ['required','min:1'],
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
            }

        }

        // $fmode_true = $request['fmode'] == 9;
        // $validator = Validator::make($request->all(),[
        //     'sys_ttrs_employee_id' => ['required'],
        //     'startdate' => ['required'],
        //     'sys_tmst_level_id' => ['required','min:1'],
        //     'sys_tmst_unit_id' => ['required','min:1'],
        //     'fmode' => ['required','min:1'],
        //     // 'enddate' => ['required', Rule::when($fmode_true, ['min:1'])],
        //     'enddate' => [Rule::requiredIf($fmode_true)]
        // ]);

        // if($validator->fails()){
        //     return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        // }

        //cek tanggal mulai aktif bekerja
        $startdate = $request->startdate;
        $startdate = Carbon::parse($startdate);
        $startdate = $startdate->copy()->startOfDay();

        $now = Carbon::parse(now());
        $now = $now->copy()->startOfDay();

        $date_diff = $now->diffInDays($startdate, false);

        //jika lebih >= dari hari ini hanya insert ke tabel sys_ttrs_promutdem
        if($date_diff >= 1) {
            try {
                $data = [];
                $data['id'] = Str::orderedUuid()->getHex();
                $data['tmpid'] = $request -> tmpid;
                $data['sys_ttrs_employee_id'] = $sys_id;
                $data['sys_tmst_company_npwp_id'] = $request -> sys_tmst_company_npwp_id;
                $data['sys_tmst_company_npp_id'] = $request -> sys_tmst_company_npp_id;
                $data['sys_tmst_cost_center_id'] = $request -> sys_tmst_cost_center_id;
                $data['absen_id'] = $request -> absen_id;
                $data['startdate'] = $request -> startdate;
                $data['enddate'] = $request -> enddate;
                $data['no_sk'] = $request -> no_sk;
                $data['sys_tmst_company_id'] = $request -> sys_tmst_company_id;
                $data['sys_tmst_level_id'] = $request -> sys_tmst_level_id;
                $data['sys_tmst_unit_id'] = $request -> sys_tmst_unit_id;
                $data['sys_tmst_grade_id'] = $request -> sys_tmst_grade_id;
                $data['sys_tmst_department_id'] = $request -> sys_tmst_department_id;
                $data['sys_tmst_department_group_divisi_id'] = $request -> sys_tmst_department_group_divisi_id;
                $data['sys_tmst_occupation_id'] = $request -> sys_tmst_occupation_id;
                $data['fmode'] = $request -> fmode;
                $data['custom2'] = $request -> custom2;
                $data['custom7'] = $request -> custom7;
                $data['description'] = $request->description;
                $data['src_origin'] = 1;
                $data['createdate'] = now();
                $data['moduser'] = '2';
                $data['moddate'] = now();

                $insert = HrPromutdem::insert($data);

                $data_status = [
                    'message'=>'Data successfully inserted.',
                    'insert_promutdem'=> $insert,
                    'data' => $data

                ];

                return response()->json($data_status, Response::HTTP_CREATED);

            } catch (QueryException $e) {
                return response()->json([
                    'message' => "Failed" . $e->errorInfo
                ]);
            }

        } else {
            //jika udah lebih <= dari hari ini, insert ke sys_ttrs_promutdem, update ke ee dan update ke occu_tree
            try {
                $data = [];
                $data['id'] = Str::orderedUuid()->getHex();
                $data['tmpid'] = $request -> tmpid;
                $data['sys_ttrs_employee_id'] = $sys_id;
                $data['sys_tmst_company_npwp_id'] = $request -> sys_tmst_company_npwp_id;
                $data['sys_tmst_company_npp_id'] = $request -> sys_tmst_company_npp_id;
                $data['sys_tmst_cost_center_id'] = $request -> sys_tmst_cost_center_id;
                $data['absen_id'] = $request -> absen_id;
                $data['startdate'] = $request -> startdate;
                $data['enddate'] = $request -> enddate;
                $data['no_sk'] = $request -> no_sk;
                $data['sys_tmst_company_id'] = $request -> sys_tmst_company_id;
                $data['sys_tmst_level_id'] = $request -> sys_tmst_level_id;
                $data['sys_tmst_unit_id'] = $request -> sys_tmst_unit_id;
                $data['sys_tmst_grade_id'] = $request -> sys_tmst_grade_id;
                $data['sys_tmst_department_id'] = $request -> sys_tmst_department_id;
                $data['sys_tmst_department_group_divisi_id'] = $request -> sys_tmst_department_group_divisi_id;
                $data['sys_tmst_occupation_id'] = $request -> sys_tmst_occupation_id;
                $data['fmode'] = $request -> fmode;
                $data['custom2'] = $request -> custom2;
                $data['custom7'] = $request -> custom7;
                $data['description'] = $request->description;
                $data['src_origin'] = 1;
                $data['createdate'] = now();
                $data['moduser'] = '2';
                $data['moddate'] = now();

                $insert = HrPromutdem::insert($data);
                //$insert = true;
                if($insert == true) {

                    $clear_sys_id = [];
                    $clear_sys_id ['sys_ttrs_employee_id'] = null;

                    $update_tree = [];
                    $update_tree['sys_ttrs_employee_id'] = $sys_id;
                    $update_tree['moddate'] = now();
                    $update_tree['src_origin'] = 1;

                    $update_ee = [];
                    $update_ee['sys_tmst_company_npwp_id'] = $request -> sys_tmst_company_npwp_id;
                    $update_ee['sys_tmst_company_npp_id'] = $request -> sys_tmst_company_npp_id;
                    $update_ee['sys_tmst_cost_center_id'] = $request -> sys_tmst_cost_center_id;
                    $update_ee['absen_id'] = $request -> absen_id;
                    $update_ee['startdate'] = $request -> startdate;
                    $update_ee['sys_tmst_company_id'] = $request -> sys_tmst_company_id;
                    $update_ee['sys_tmst_level_id'] = $request -> sys_tmst_level_id;
                    $update_ee['sys_tmst_unit_id'] = $request -> sys_tmst_unit_id;
                    $update_ee['sys_tmst_grade_id'] = $request -> sys_tmst_grade_id;
                    $update_ee['sys_tmst_department_id'] = $request -> sys_tmst_department_id;
                    $update_ee['sys_tmst_department_group_divisi_id'] = $request -> sys_tmst_department_group_divisi_id;
                    $update_ee['sys_tmst_occupation_id'] = $request -> sys_tmst_occupation_id;
                    $update_ee['custom2'] = $request -> custom2;
                    $update_ee['custom7'] = $request -> custom7;
                    $update_ee['src_origin'] = 1;
                    $update_ee['moddate'] = now();


                    $clear_occu_tree = OccuTree::where('sys_ttrs_employee_id', '=', $sys_id)
                                     ->update($clear_sys_id);

                    $update_data_ee = EmployeeDB::where('nip','=', $request->nip)
                                    ->update($update_ee);

                    if ($id_occu_tree == true) {

                    $update_occu_tree = OccuTree::where('sys_tmst_occupation_id', '=', $request->sys_tmst_occupation_id)
                                    ->update($update_tree);
                    }
                }

                $update_data_ee = ($update_data_ee = 1 ? true : false);
                $update_occu_tree = ($update_occu_tree = 1 ? true : false);

                $data_status = [
                    'message'=>'Data successfully inserted.',
                    'status_insert_promutdem'   => $insert,
                    'status_update_employee'    => $update_data_ee,
                    'status_update_occutree'   => $update_occu_tree,
                    'status_clear_occu_tree'   => $clear_occu_tree,
                    'data_insert_promutdem'     => $data,
                    'data_update_employee'     => $update_ee,
                    'data_update_occutree'     => $update_tree
                ];

                return response()->json($data_status, Response::HTTP_CREATED);

            } catch (QueryException $e) {
                return response()->json([
                    'message' => "Failed" . $e->errorInfo
                ]);
            }

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HrPromutdem  $hrPromutdem
     * @return \Illuminate\Http\Response
     */
    public function show(HrPromutdem $hrPromutdem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HrPromutdem  $hrPromutdem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HrPromutdem $hrPromutdem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HrPromutdem  $hrPromutdem
     * @return \Illuminate\Http\Response
     */
    public function destroy(HrPromutdem $hrPromutdem)
    {
        //
    }
}
