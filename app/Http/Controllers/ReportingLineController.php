<?php

namespace App\Http\Controllers;

use App\Models\OccuTree;
use App\Models\Occupation;
use Illuminate\Http\Request;
use App\Models\ReportingLine;
use Facade\FlareClient\Report;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\ResponseBuilder;
use App\Http\Helper\ResponseBuilderList;
use Symfony\Component\HttpFoundation\Response;


class ReportingLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // SELECT e.nip,e.firstname,o.alias as occu_name,e2.nip as nip_upline,e2.firstname name_upline,o2.alias as occu_name_upline from sys_vtrs_employee_upline_detail_data u
        // INNER JOIN sys_ttrs_employee e on e.id=u.id
        // INNER JOIN sys_ttrs_employee e2 on e2.id=u.reporting_line_id
        // INNER JOIN sys_tmst_occupation o on o.id=e.sys_tmst_occupation_id
        // INNER JOIN sys_tmst_occupation o2 on o2.id=e2.sys_tmst_occupation_id
        // where 1=1
        // -- and e.nip='ID011407441'

        $data = ReportingLine::from('sys_vtrs_employee_upline_detail_data_api as u')
            ->selectRaw('
                            e.nip as e_nip,
                            e.firstname as e_name,
                            o.alias as e_occu,
                            e.sys_tmst_occupation_id as e_occu_code,
                            e2.nip as upline_nip,
                            e2.firstname upline_name,
                            o2.alias as upline_occu,
                            u.reporting_line_occu_id as upline_occu_code
                        ')
            ->join('sys_ttrs_employee as e', 'e.id', '=', 'u.id')
            ->join('sys_ttrs_employee as e2', 'e2.id', '=', 'u.reporting_line_id')
            ->join('sys_tmst_occupation as o', 'o.id', '=', 'e.sys_tmst_occupation_id')
            ->join('sys_tmst_occupation as o2', 'o2.id', '=', 'e2.sys_tmst_occupation_id')
            ->get();

        $status = true;
        $message  = "Data  ditemukan.";
        $response_code = Response::HTTP_FOUND;
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, $nip)
    {
        $occupation = $nip;

        // $data = ReportingLine::from('sys_vtrs_employee_upline_detail_data as u')
        // ->selectRaw('e.nip as e_nip,e.firstname as e_name,o.alias as e_occu,e2.nip as upline_nip,e2.firstname upline_name,o2.alias as upline_occu')
        // ->join('sys_ttrs_employee as e', 'e.id', '=', 'u.id')
        // ->join('sys_ttrs_employee as e2', 'e2.id', '=', 'u.reporting_line_id')
        // ->join('sys_tmst_occupation as o', 'o.id', '=', 'e.sys_tmst_occupation_id')
        // ->join('sys_tmst_occupation as o2', 'o2.id', '=', 'e2.sys_tmst_occupation_id')
        // ->where('e.nip','=', $nip)
        // ->get();

        $data = ReportingLine::from('sys_vtrs_employee_upline_detail_data_api as u')
            ->selectRaw('
                            e.nip as e_nip,
                            e.firstname as e_name,
                            o.alias as e_occu,
                            e.sys_tmst_occupation_id as e_occu_code,
                            e2.nip as upline_nip,
                            e2.firstname upline_name,
                            o2.alias as upline_occu,
                            u.reporting_line_occu_id as upline_occu_code
                        ')
            ->join('sys_ttrs_employee as e', 'e.id', '=', 'u.id')
            ->join('sys_ttrs_employee as e2', 'e2.id', '=', 'u.reporting_line_id')
            ->join('sys_tmst_occupation as o', 'o.id', '=', 'e.sys_tmst_occupation_id')
            ->join('sys_tmst_occupation as o2', 'o2.id', '=', 'e2.sys_tmst_occupation_id')
            ->where('e.nip','=', $nip)
            ->get();

        $status = true;
        $message  = "Data  ditemukan.";
        $response_code = Response::HTTP_FOUND;

        return ResponseBuilder::result($status, $message, $data, $response_code);

    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReportingLine  $reportingLine
     * @return \Illuminate\Http\Response
     */
    public function show(ReportingLine $reportingLine)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReportingLine  $reportingLine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportingLine $reportingLine)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReportingLine  $reportingLine
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportingLine $reportingLine)
    {
        //
    }
}
