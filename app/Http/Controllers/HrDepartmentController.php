<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Models\HrDepartment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\ResponseBuilder;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\QueryException;
use App\Http\Helper\ResponseBuilderList;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class HrDepartmentController extends Controller
{
    public function index()
    {

        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        // $data = EmployeeDB::where('nip', 'ID011407441')->first();
        $data = HrDepartment::all();
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);

        //return HrDepartment::paginate(15);
        // $model = new HrDepartment();
        // $tst = $model->getFillable();

        // $slistdata = join("','",$tst);
        // $listdata = sprintf("'%s'",$slistdata);

        // $fillable = $hrDepartment->getFillable();
        // $listdata = "`".join("`,`",$fillable)."`";
        // $users = DB::table('sys_tmst_department')
        // ->select(DB::raw($listdata))
        // ->get();

        // return ($users);

    }


    public function deptjson () {

        $data = HrDepartment::where('parent', 'root')->with('children')->get();

        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);

    }




    public function adddivisi(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name' => ['required','min:2'],
            'tmpid' => ['required','min:2']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }


        // untuk id level root
        // SELECT departement_id
        // FROM sys_tmst_department
        // WHERE 1=1
        // and LENGTH(departement_id) = 2
        // ORDER BY id DESC
        // limit 1

        $maxid = (HrDepartment::whereRaw('LENGTH(departement_id) = 2')
                    ->orderBy('id','DESC')
                    ->limit(1)
                    ->value('departement_id')) + 1 ;

        $maxid_table = (HrDepartment::max('id')) + 1 ;

        $data = [];
        //$data['id'] = Str::uuid()->__toString();
        $data['id'] = $maxid_table;
        $data['sys_tmst_company_id'] = 1;
        $data['departement_id'] = $maxid;
        $data['alias_code'] = $request-> alias_code;
        $data['name'] = $request-> name;
        $data['parent'] = 'root';
        $data['cost_center'] =  $request-> cost_center;
        $data['group_id'] = 1; //$request-> group_id;
        $data['description'] = $request-> description;
        $data['src_origin'] = 1;
        $data['createdate'] = now();
        $data['moduser'] = '2';
        $data['moddate'] = now();


        $insert = HrDepartment::insert($data);
        //(Boolean)$insert = true;

        $response = [
            'message'   =>'Data successfully inserted.',
            'status'    => $insert,
            'data'      => $data
        ];

        return response()->json($response, Response::HTTP_CREATED);

    }

    public function adddept(Request $request) {

        $validator = Validator::make($request->all(),[
            'name' => ['required','min:2'],
            'departement_id' => ['required','between:2,3']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        // untuk mencari id level 2
        // SELECT departement_id
        // FROM sys_tmst_department
        // WHERE 1=1
        // and LENGTH(departement_id) = 5
        // and LEFT(departement_id,2) = '11'
        // ORDER BY id DESC
        // limit 1

        $max_departement_id = (HrDepartment::whereRaw('LENGTH(departement_id) = 5')
                ->whereRaw('LEFT(departement_id,2) = ?', $request->departement_id )
                ->orderBy('id','DESC')
                ->limit(1)
                ->value('departement_id')) + 1;

        if ($max_departement_id == 1 ) {
            $max_departement_id = $request->departement_id . "001";
        } else {
            $max_departement_id = $max_departement_id;
        }

        $maxid_table = (HrDepartment::max('id')) + 1 ;

        // SELECT id
        // FROM sys_tmst_department
        // WHERE 1=1
		// 		and LENGTH(departement_id) = 2
        // and LEFT(departement_id,2) = 14
        // ORDER BY id DESC
        // limit 1

        $parent_id = HrDepartment::whereRaw('LENGTH(departement_id) = 2')
        ->whereRaw('LEFT(departement_id,2) = ?', $request->departement_id )
        ->orderBy('id','DESC')
        ->limit(1)
        ->value('id');

        $data = [];
        $data['id'] = $maxid_table;
        $data['sys_tmst_company_id'] = 1;
        $data['departement_id'] = $max_departement_id ;
        $data['alias_code'] = $request-> alias_code;
        $data['name'] = $request-> name;
        $data['parent'] = $parent_id;
        $data['cost_center'] =  $request-> cost_center;
        $data['group_id'] = 1; //$request-> group_id;
        $data['description'] = $request-> description;
        $data['src_origin'] = 1;
        $data['createdate'] = now();
        $data['moduser'] = '2';
        $data['moddate'] = now();

        //dd($data);

        $insert = HrDepartment::insert($data);

        $response = [
            'message'=>'Data successfully inserted.',
            'status'=> $insert,
            'data' => $data
        ];

        return response()->json($response, Response::HTTP_CREATED);

    }

    public function addsection(Request $request) {

        $validator = Validator::make($request->all(),[
            'name' => ['required','min:2'],
            'departement_id' => ['required','between:5,6']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // untuk id level 3
        // SELECT departement_id
        // FROM sys_tmst_department
        // WHERE 1=1
        // and LENGTH(departement_id) = 8
        // and LEFT(departement_id,5) = '11001'
        // ORDER BY id DESC
        // limit 1

        $max_departement_id = (HrDepartment::whereRaw('LENGTH(departement_id) = 8')
        ->whereRaw('LEFT(departement_id,5) = ?', $request->departement_id )
        ->orderBy('id','DESC')
        ->limit(1)
        ->value('departement_id')) + 1;

        if ($max_departement_id == 1 ) {
            $max_departement_id = $request->departement_id . "001";
        } else {
            $max_departement_id = $max_departement_id;
        }

        // $max_departement_id = DB::table('sys_tmst_department')
        // ->selectRaw('MAX(departement_id) as max_dept_id')
        // ->whereRaw('LEFT(departement_id,5) = ?', $request->departement_id )
        // ->whereRaw('LENGTH(departement_id) = 8')
        // ->get()->toArray();

        // $max_dept_idd = ($max_departement_id['0']['max_dept_id']);


        // dd($max_dept_idd);

        $maxid_table = (HrDepartment::max('id')) + 1 ;

        // SELECT id
        // FROM sys_tmst_department
        // WHERE 1=1
		// 		and LENGTH(departement_id) = 5
        // and LEFT(departement_id,5) = 14009
        // ORDER BY id DESC
        // limit 1

        $parent_id = HrDepartment::whereRaw('LENGTH(departement_id) = 5')
        ->whereRaw('LEFT(departement_id,5) = ?', $request->departement_id )
        ->orderBy('id','DESC')
        ->limit(1)
        ->value('id');

        $data = [];
        $data['id'] = $maxid_table;
        $data['sys_tmst_company_id'] = 1;
        $data['departement_id'] = $max_departement_id ;
        $data['alias_code'] = $request-> alias_code;
        $data['name'] = $request-> name;
        $data['parent'] = $parent_id;
        $data['cost_center'] =  $request-> cost_center;
        $data['group_id'] = 1; //$request-> group_id;
        $data['description'] = $request-> description;
        $data['src_origin'] = 1;
        $data['createdate'] = now();
        $data['moduser'] = '2';
        $data['moddate'] = now();

        $insert = HrDepartment::insert($data);

        $response = [
            'message'=>'Data successfully inserted.',
            'status'=> $insert,
            'data' => $data
        ];

        return response()->json($response, Response::HTTP_CREATED);

    }

    public function search() {

        $query = HrDepartment::whereRaw('LENGTH(departement_id) = 8');

        // $userQuery = QueryBuilder::for($query) // start from an existing Builder instance
        // ->withTrashed() // use your existing scopes
        // ->allowedIncludes('posts', 'permissions')
        // ->where('score', '>', 42); // chain on any of Laravel's query builder methods

        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;

        $data = QueryBuilder::for($query)
        ->allowedFilters(['name'])
        ->get();

        if (empty($data)){
            $message  = "Data kosong";
            return ResponseBuilder::result('False', $message, '[]', '404');
        }

        return ResponseBuilder::result($status, $message, $data, $response_code);

    }
}
