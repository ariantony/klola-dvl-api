<?php

namespace App\Http\Controllers;

use App\Models\OccuTree;
use App\Models\Occupation;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Database\QueryException;
use App\Http\Helper\ResponseBuilderList;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class OccupationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = Occupation::all();
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        if ($request->action == 'child') {
            $validator = Validator::make($request->all(),[
                'alias' => ['required'],
                'sys_tmst_occupation_id' => ['required'],
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } else {

            $validator = Validator::make($request->all(),[
                'alias' => ['required']
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
            }

        }

        $maxid_table = (Occupation::max('altid')) + 1;
        $max_tmpid = (OccuTree::max('tmpid')) + 1;
        // $maxid_table = (DB::table('sys_tmst_occupation')
        // ->selectRaw('MAX(altid)')
        // ->value('altid')) + 1 ;

        // SELECT MAX(`code`) as code
        // FROM sys_tmst_occupation_tree
        // WHERE 1=1
        // and LENGTH(`code`) = 3

        if ($request->action == 'root') {

            $maxidcode = (DB::connection('mysql_api')->table('sys_tmst_occupation_tree')
            ->selectRaw('MAX(`code`)')
            ->whereRaw('LENGTH(`code`) = 3')
            ->value('`code`')) + 1 ;

            $maxidcode1 = str_pad($maxidcode, 3, '0', STR_PAD_LEFT);

            $occu = [];
            $occu['id'] = "dvl_". $maxid_table;
            $occu['sys_tmst_company_id'] = 1;
            $occu['code'] = "dvl_". $maxid_table;
            $occu['altid'] = $maxid_table;
            $occu['alias'] = $request -> alias;
            $occu['description'] = $request -> description;
            $occu['createdate'] = now();
            $occu['moduser'] = '2';
            $occu['moddate'] = now();

            $occutree = [];
            $occutree['id'] = Str::orderedUuid()->getHex();
            $occutree['sys_tmst_company_id'] = 1;
            $occutree['sys_tmst_occupation_id'] = "dvl_". $maxid_table;
            $occutree['tmpid'] = $max_tmpid;
            $occutree['alias'] = $request -> alias;
            $occutree['code'] = $maxidcode1;
            $occutree['parent'] = 'root';
            $occutree['parent_tmpid'] = 0;
            $occutree['active_status'] = 1;
            $occutree['src_origin'] = 1;
            $occutree['tid'] = 1;
            $occutree['createdate'] = now();
            $occutree['moduser'] = '2';
            $occutree['moddate'] = now();

            $insert_tree = OccuTree::insert($occutree);
            $insert_occu = Occupation::insert($occu);

            $response = [
                'message'=>'Data successfully inserted.',
                'status_insert_occutree'=> $insert_tree,
                'status_insert_occu'=> $insert_occu,
                'data_insert_occu'=> $occu,
                'data_insert_tree'=> $occutree
            ];

            return response()->json($response, Response::HTTP_CREATED);

        } elseif ($request->action == 'child') {

            // SELECT MAX(`code`) as `code`
            // FROM sys_tmst_occupation_tree
            // WHERE 1=1
            // and LENGTH(`code`) = 6
            // and LEFT(`code`,3) = '005'
            // ORDER BY `code` DESC

            #  Belum dapat dapat nilainya jika karakternya panjang
            // $lencode = strlen($request->code);
            // $childcode = $lencode + 3;

            // $maxidcode = DB::table('sys_tmst_occupation_tree')
            // ->selectRaw('MAX(`code`)')
            // ->whereRaw('LENGTH(`code`) = ?', $childcode )
            // ->whereRaw('LEFT(`code`,? ) = ?', [$lencode, $request->code])
            // ->value('`code`');

            // if ($maxidcode == null) {
            //     $request->code . "001";
            // }

            // $maxright = intval(substr($maxidcode, -3)) + 1;
            // $maxright = str_pad($maxright, 3, '0', STR_PAD_LEFT);
            // $maxright1 = $request->code . $maxright;

            $parent_id = DB::connection('mysql_api')->table('sys_tmst_occupation_tree')
                        ->whereRaw('sys_tmst_occupation_id = ?', $request->sys_tmst_occupation_id)
                        ->value('id');
            //dd($parent_id);

            $parent_tmpid = DB::connection('mysql_api')->table('sys_tmst_occupation_tree')
                        ->whereRaw('sys_tmst_occupation_id = ?', $request->sys_tmst_occupation_id)
                        ->value('parent_tmpid');

            //dd($parent_tmpid);

            $occu = [];
            $occu['id'] = "dvl_". $maxid_table;
            $occu['sys_tmst_company_id'] = 1;
            $occu['code'] = "dvl_". $maxid_table;
            $occu['altid'] = $maxid_table;
            $occu['alias'] = $request -> alias;
            $occu['description'] = $request -> description;
            $occu['createdate'] = now();
            $occu['moduser'] = '2';
            $occu['moddate'] = now();

            $occutree = [];
            $occutree['id'] = Str::orderedUuid()->getHex();
            $occutree['sys_tmst_company_id'] = 1;
            $occutree['tmpid'] = $max_tmpid;
            $occutree['sys_tmst_occupation_id'] = "dvl_". $maxid_table;
            $occutree['alias'] = $request -> alias;
            $occutree['code'] = null;
            $occutree['parent'] = $parent_id;
            $occutree['parent_tmpid'] = $parent_tmpid;
            $occutree['src_origin'] = 1;
            $occutree['active_status'] = 1;
            $occutree['tid'] = 1;
            $occutree['createdate'] = now();
            $occutree['moduser'] = '2';
            $occutree['moddate'] = now();

            $insert_tree = OccuTree::insert($occutree);
            $insert_occu = Occupation::insert($occu);

            $response = [
                'message'=>'Data successfully inserted.',
                'status_insert_occutree'=> $insert_tree,
                'status_insert_occu'=> $insert_occu,
                'data_insert_occu'=> $occu,
                'data_insert_tree'=> $occutree
            ];

            return response()->json($response, Response::HTTP_CREATED);

        } else {

            $status = false;
            $message  = "Data tidak ditemukan.";
            $response_code = Response::HTTP_FORBIDDEN;

            $data = [];

            return ResponseBuilder::result($status, $message, $data, $response_code);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Occupation  $occupation
     * @return \Illuminate\Http\Response
     */
    public function show(Occupation $occupation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Occupation  $occupation
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, $nip)
    {
        $katakunci = $request->occu_name;

        $parent_tree = OccuTree::from('sys_tmst_occupation_tree as t')
        ->join('sys_ttrs_employee as e', 'e.id','=', 't.sys_ttrs_employee_id')
        ->selectRaw('t.id')
        ->whereRaw('e.nip= ?', $nip)->get()->toArray();


        $cari = OccuTree::from('sys_tmst_occupation_tree as t')
                ->join('sys_tmst_occupation as o', 'o.id', '=','t.sys_tmst_occupation_id')
                ->leftJoin('sys_ttrs_employee as e','e.id','=','t.sys_ttrs_employee_id')
                ->selectRaw('t.id, o.alias as occu_name, e.nip, t.code, t.tmpid, t.sys_tmst_occupation_id, t.parent_tmpid')
                ->where('t.parent', '=', $parent_tree )
                ->where('o.alias', 'like', "%" . $katakunci . "%")
                ->whereRaw('(LENGTH(t.sys_ttrs_employee_id) < 2 OR t.sys_ttrs_employee_id is null)')
                ->get();

                $status = true;
                $message  = "Data  ditemukan.";
                $response_code = Response::HTTP_FOUND;

        return ResponseBuilder::result($status, $message, $cari, $response_code);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Occupation  $occupation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Occupation $occupation)
    {
        //
    }
}
