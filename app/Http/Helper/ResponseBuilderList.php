<?php
namespace App\Http\Helper;

class ResponseBuilderList {

    public static function result($status="", $message="", $data="", $count="", $response_code="" ) {

        return response()->json([
            "status" => $status,
            "message" => $message,
            "rows" => $count,
            "data" => $data,
        ], $response_code);

    }
}

?>
