<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrPromutdem extends Model
{
    use HasFactory;
    protected $connection = 'mysql_api';

    public $table = 'sys_ttrs_promutdem';

    protected $fillable = [
        'id',
        'sys_ttrs_employee_id',
        'sys_tmst_company_npwp_id',
        'sys_tmst_company_npp_id',
        'sys_tmst_cost_center_id',
        'absen_id',
        'startdate',
        'startwork',
        'enddate',
        'no_sk',
        'sys_tmst_company_id',
        'sys_tmst_level_id',
        'sys_tmst_unit_id',
        'sys_tmst_grade_id',
        'sys_tmst_department_id',
        'sys_tmst_department_group_divisi_id',
        'sys_tmst_occupation_id',
        'fmode',
        'custom2',
        'custom7',
        'createdate',
        'moduser',
        'moddate',
        'tmpid',
        'src_origin',

    ];

    // protected $guarded = [];

    protected $hidden = [
        'journal_code',
        'username_employee',
        'mail_office',
        'nip',
        'extenddate',
        'sys_tmst_company_id_move',
        'sys_tmst_kpp_id',
        'sys_tmst_kpp_id_old',
        'sys_tmst_occupation_tree_id',
        'sys_tmst_timeshift_id',
        'last_employee_status',
        'pph_resume',
        'pph_pajak',
        'jkk_code',
        'jkk_value',
        'period',
        'employee_status',
        'doc_status',
        'approval_status',
        'fstatus',
        'custom1',
        'custom3',
        'custom4',
        'custom5',
        'custom6',
        'custom8',
        'custom9',
        'custom10',
        'name',
        'description',
        'upload_code',
        'update_status'
    ];

    protected $casts = [
        'startdate' => 'datetime:Y-m-d',
        'enddate' => 'datetime:Y-m-d',
        'extenddate' => 'datetime:Y-m-d',
        'startwork' => 'datetime:Y-m-d',
        'createdate' => 'datetime:Y-m-d H:i:s',
        'moddate' => 'datetime:Y-m-d H:i:s'
    ];
}
