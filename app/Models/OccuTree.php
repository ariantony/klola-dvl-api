<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OccuTree extends Model
{
    use HasFactory;

    protected $connection = 'mysql_api';

    public $table = 'sys_tmst_occupation_tree';

    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'id',
        'sys_tmst_company_id',
        'sys_tmst_occupation_id',
        'tmpid',
        'alias',
        'code',
        'parent',
        'parent_tmpid',
        'active_status',
        'src_origin',
        'tid',
        'createdate',
        'moduser',
        'moddate'

    ];

    protected $guarded = [
        'sys_ttrs_employee_id'
    ];

    protected $hidden = [
        'sys_tmst_company_id',
        'sys_tmst_grade_id',
        'sys_tmst_department_id',
        'createdate',
        'moduser',
        'moddate'
    ];

    protected $casts = [
        'createdate' => 'datetime:Y-m-d H:i:s',
        'moddate' => 'datetime:Y-m-d H:i:s'
    ];
}
