<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportingLine extends Model
{
    use HasFactory;

    protected $connection = 'mysql_api';

    public $table = 'sys_vtrs_employee_upline_detail_data_api';
    public $incrementing = false;

    // protected $fillable = [
    //     'id',
    //     'sys_tmst_company_id',
    //     'sys_tmst_occupation_id',
    //     'alias',
    //     'code',
    //     'parent',
    //     'active_status',
    //     'tid',
    //     'createdate',
    //     'moduser',
    //     'moddate',
    //     'tmpid',
    //     'parent_tmpid',
    //     'src_origin'

    // ];

    protected $guarded = [

    ];

    // protected $hidden = [
    //     'id',
    //     'sys_tmst_company_id',
    //     'sys_tmst_occupation_id',
    //     'active_status',
    //     'tid',
    //     'createdate',
    //     'moduser',
    //     'moddate',
    //     'alias'
    // ];

    // protected $casts = [
    //     'createdate' => 'datetime:Y-m-d H:i:s',
    //     'moddate' => 'datetime:Y-m-d H:i:s'
    // ];

    // public function parent() {
    //     return $this->belongsTo(self::class, 'parent_tmpid');
    // }

    // public function children() {
    //     return $this->hasMany(self::class, 'parent_tmpid')->with('children');
    //     // return $this->hasMany(self::class, 'parent_tmpid');
    // }
}
