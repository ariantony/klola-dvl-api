<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    use HasFactory;

    protected $connection = 'mysql_api';

    public $table = 'sys_tmst_occupation';

    protected $fillable = [
        'id',
        'sys_tmst_company_id',
        'sys_tmst_grade_id',
        'sys_tmst_department_id',
        'code',
        'altid',
        'gid',
        'alias',
        'description',
        'createdate',
        'moduser',
        'moddate'

    ];

    protected $guarded = [

    ];

    protected $hidden = [
        'sys_tmst_company_id',
        'sys_tmst_grade_id',
        'sys_tmst_department_id',
        'createdate',
        'moduser',
        'moddate'
    ];

    protected $casts = [
        'createdate' => 'datetime:Y-m-d H:i:s',
        'moddate' => 'datetime:Y-m-d H:i:s'
    ];

}
