<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    use HasFactory;
    protected $connection = 'mysql_api';

    public $table = 'sys_ttrs_employee_hr_pay';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'sys_ttrs_employee_id',
        'ftype',
        'pay_method',
        'account_name',
        'account_no',
        'sys_tmst_bank_id',
        'branch',
        'account_type',
        'status',
        'attribute',
        'description',
        'createdate',
        'moduser',
        'moddate'

    ];

    protected $guarded = [

    ];

    protected $hidden = [
        'attribute',
        'description',
        'createdate',
        'moduser',
        'moddate'

    ];

    protected $casts = [
        'createdate' => 'datetime:Y-m-d H:i:s',
        'moddate' => 'datetime:Y-m-d H:i:s'
    ];
}
