<?php

use App\Http\Controllers\BankAccountController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HrPHKController;
use App\Http\Controllers\EmployeeDBController;
use App\Http\Controllers\HrDepartmentController;
use App\Http\Controllers\HrPromutdemController;
use App\Http\Controllers\HrRecruitmentController;
use App\Http\Controllers\OccupationController;
use App\Http\Controllers\ReportingLineController;
use App\Models\BankAccount;

/*
    |--------------------------------------------------------------------------
    | API Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register API routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | is assigned the "api" middleware group. Enjoy building your API!
    |
*/

Route::group(
    [
        'middleware' => 'api',
        'namespace'  => 'App\Http\Controllers',
        'prefix'     => 'auth',
    ],
    function ($router) {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'AuthController@register');
        Route::post('logout', 'AuthController@logout');
        Route::get('profile', 'AuthController@profile');
        Route::post('refresh', 'AuthController@refresh');
    }
);

Route::group(
    [
        //'middleware' => 'api',
        'namespace'  => 'App\Http\Controllers',
        'prefix'     => 'emp',
    ],
    function ($router) {
        // Route::resource('todos', 'TodoController');
        Route::get('recruitment', [HrRecruitmentController::class, 'index']);
        Route::post('recruitment', [HrRecruitmentController::class, 'store']);


        Route::put('employeedb/{nip}', [EmployeeDBController::class, 'update']);
        Route::get('employeedb/{nip}', [EmployeeDBController::class,'show']);
        Route::get('employee/search', [EmployeeDBController::class,'search']);
        Route::post('employeedb', [EmployeeDBController::class,'store']);
        Route::get('employeedb', [EmployeeDBController::class,'index']);
        Route::get('employee-all', [EmployeeDBController::class,'all']);


        Route::get('promutdem', [HrPromutdemController::class, 'index']);
        Route::post('promutdem', [HrPromutdemController::class, 'store']);

        Route::get('phk', [HrPHKController::class, 'index']);
        Route::post('phk', [HrPHKController::class, 'store']);
        Route::put('phk/{nip}', [HrPHKController::class, 'update']);

        Route::get('dept', [HrDepartmentController::class, 'index']);
        Route::get('dept-json', [HrDepartmentController::class, 'deptjson']);
        Route::get('dept/search', [HrDepartmentController::class, 'search']);
        Route::post('divisi', [HrDepartmentController::class, 'adddivisi']);
        Route::post('dept', [HrDepartmentController::class, 'adddept']);
        Route::post('section', [HrDepartmentController::class, 'addsection']);

        Route::get('occupation', [OccupationController::class, 'index']);
        Route::post('occupation', [OccupationController::class, 'store']);
        Route::get('occupation/search/{nip}', [OccupationController::class, 'search']);

        Route::get('reportingline', [ReportingLineController::class, 'index']);
        Route::get('reportingline/search/{nip}', [ReportingLineController::class, 'search']);


        Route::get('bank-account', [BankAccountController::class, 'index']);
        Route::post('bank-account', [BankAccountController::class, 'store']);
        Route::delete('bank-account/{id}', [BankAccountController::class, 'destroy']);
        Route::put('bank-account/{id}', [BankAccountController::class, 'update']);
    }
);


