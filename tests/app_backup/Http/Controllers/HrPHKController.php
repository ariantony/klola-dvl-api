<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\ResponseBuilder;
use App\Models\EmployeeDB;
use App\Models\HrPHK;
use Symfony\Component\HttpFoundation\Response;

class HrPHKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = HrPHK::paginate(50);

        return ResponseBuilder::result($status, $message, $data, $response_code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $eedb = EmployeeDB::where('id', '=', $request->sys_ttrs_employee_id)
                    ->get()
                    ->toArray();


            foreach ($eedb as $empdb) {
                $data = [];
                $data['id'] = Str::uuid()->__toString();
                $data['tmpid'] = Str::uuid()->__toString();
                $data['sys_tmst_company_id'] = '1';
                $data['sys_ttrs_employee_id'] = $request-> sys_ttrs_employee_id;
                $data['no_sk'] = $request-> no_sk;
                $data['sys_tmst_alasan_phk_id'] =  $request-> sys_tmst_alasan_phk_id;
                $data['apply_date_phk'] = $request-> apply_date_phk;
                $data['not_active_work_date'] = $request-> not_active_work_date;
                $data['last_employee_status'] = $empdb['employee_status'];
                $data['description'] = $request-> description;
                $data['fmode'] = '4';
                $data['fstatus'] = '0';
                $data['createdate'] = now();
                $data['moduser'] = '2';
                $data['moddate'] = now();

            }

            $insert = DB::table('sys_ttrs_employee_phk')->insert($data);

            $response = [
                'message'=>'Data successfully inserted.',
                'status'=> $insert
            ];

            return response()->json($response, Response::HTTP_CREATED);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HrPHKController  $hrPHKController
     * @return \Illuminate\Http\Response
     */
    public function show(HrPHK $hrPHK)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HrPHKController  $hrPHKController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tmpid)
    {
        $insert = DB::table('sys_ttrs_employee_phk')
            ->where('tmpid','=', $tmpid )
            ->update([
                'no_sk' => $request -> no_sk,
                'sys_tmst_alasan_phk_id' => $request -> sys_tmst_alasan_phk_id,
                'apply_date_phk' => $request -> apply_date_phk,
                'not_active_work_date' => $request -> not_active_work_date,
                'description' => $request -> description,
                'moddate' => now()
            ]);

        $status_insert = ($insert == 1 ? true : false);

        $response = [
            'message'=>'Data berhasil diupdate.',
            'status'=> $status_insert
        ];

        return response()->json($response, Response::HTTP_CREATED);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HrPHKController  $hrPHKController
     * @return \Illuminate\Http\Response
     */
    public function destroy(HrPHK $hrPHK)
    {
        //
    }
}
