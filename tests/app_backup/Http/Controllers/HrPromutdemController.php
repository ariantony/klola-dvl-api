<?php

namespace App\Http\Controllers;

use App\Models\HrPromutdem;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class HrPromutdemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = HrPromutdem::paginate(15);

        return ResponseBuilder::result($status, $message, $data, $response_code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $fmode_true = $request['fmode'] == 9;


        $validator = Validator::make($request->all(),[
            'sys_ttrs_employee_id' => ['required'],
            'startdate' => ['required'],
            'sys_tmst_level_id' => ['required','min:1'],
            'sys_tmst_unit_id' => ['required','min:1'],
            'fmode' => ['required','min:1'],
            // 'enddate' => ['required', Rule::when($fmode_true, ['min:1'])],
            'enddate' => [Rule::requiredIf($fmode_true)]
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {

            $data = [];
            $data['id'] = Str::uuid()->__toString();
            $data['sys_ttrs_employee_id'] = $request -> sys_ttrs_employee_id;
            $data['sys_tmst_company_npwp_id'] = $request -> sys_tmst_company_npwp_id;
            $data['sys_tmst_company_npp_id'] = $request -> sys_tmst_company_npp_id;
            $data['sys_tmst_cost_center_id'] = $request -> sys_tmst_cost_center_id;
            $data['absen_id'] = $request -> absen_id;
            $data['startdate'] = $request -> startdate;
            $data['enddate'] = $request -> enddate;
            $data['no_sk'] = $request -> no_sk;
            $data['sys_tmst_company_id'] = $request -> sys_tmst_company_id;
            $data['sys_tmst_level_id'] = $request -> sys_tmst_level_id;
            $data['sys_tmst_unit_id'] = $request -> sys_tmst_unit_id;
            $data['sys_tmst_grade_id'] = $request -> sys_tmst_grade_id;
            $data['sys_tmst_department_id'] = $request -> sys_tmst_department_id;
            $data['sys_tmst_department_group_divisi_id'] = $request -> sys_tmst_department_group_divisi_id;
            $data['sys_tmst_occupation_id'] = $request -> sys_tmst_occupation_id;
            $data['fmode'] = $request -> fmode;
            $data['custom2'] = $request -> custom2;
            $data['custom7'] = $request -> custom7;
            $data['description'] = $request->description;
            $data['createdate'] = now();
            $data['moduser'] = '2';
            $data['moddate'] = now();

            $insert = DB::table('sys_ttrs_promutdem')->insert($data);

            $response = [
                'message'=>'Data successfully inserted.',
                'status'=> $insert
            ];

            return response()->json($response, Response::HTTP_CREATED);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HrPromutdem  $hrPromutdem
     * @return \Illuminate\Http\Response
     */
    public function show(HrPromutdem $hrPromutdem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HrPromutdem  $hrPromutdem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HrPromutdem $hrPromutdem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HrPromutdem  $hrPromutdem
     * @return \Illuminate\Http\Response
     */
    public function destroy(HrPromutdem $hrPromutdem)
    {
        //
    }
}
