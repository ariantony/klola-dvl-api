<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\HrRecruitment;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\ResponseBuilder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class HrRecruitmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
   {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = HrRecruitment::paginate(15);

        return ResponseBuilder::result($status, $message, $data, $response_code);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'startwork' => ['required'],
            'tax_month' => ['required'],
            'sys_tmst_company_id' => ['required','numeric','min:1','max:9'],
            'sys_tmst_level_id' => ['required','numeric','min:1'],
            'sys_tmst_unit_id' => ['required','numeric','min:1','max:8'],
            'sys_tmst_grade_id' => ['required','numeric','min:1','max:8'],
            'sys_tmst_department_id' => ['required','numeric','min:1'],
            'sys_tmst_department_group_divisi_id' => ['required','numeric','min:1','max:8'],
            'sys_tmst_occupation_id' => ['required','min:1','max:32'],
            'sys_tmst_cost_center_id' => ['required','min:1','max:32'],
            'jkk_code' => ['required','min:2','max:32'],
            'custom1' => ['required','min:2','max:32'],
            'custom2' => ['required','min:2','max:32'],
            'custom3' => ['required','min:2','max:32'],
            'custom7' => ['required','min:2','max:32']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $data = array();
            $data['id'] = Str::uuid()->__toString();
            $data['sys_ttrs_employee_id'] = $request->sys_ttrs_employee_id;
            $data['nip'] = $request->nip;
            $data['tax_npwp_personal'] = $request->tax_npwp_personal;
            $data['startdate'] = $request->startdate;
            $data['enddate'] = $request->enddate;
            $data['extenddate'] = $request->extenddate;
            $data['startwork'] = $request->startwork;
            $data['tax_month'] = $request->tax_month;
            $data['no_sk'] = $request->no_sk;
            $data['employee_status'] = $request->fmode;
            $data['last_employee_status'] = $request->last_employee_status;
            $data['sys_tmst_kpp_id'] = $request->sys_tmst_kpp_id;
            $data['sys_tmst_company_id'] = $request->sys_tmst_company_id;
            $data['sys_tmst_level_id'] = $request->sys_tmst_level_id;
            $data['sys_tmst_unit_id'] = $request->sys_tmst_unit_id;
            $data['sys_tmst_grade_id'] = $request->sys_tmst_grade_id;
            $data['sys_tmst_department_id'] = $request->sys_tmst_department_id;
            $data['sys_tmst_department_group_divisi_id'] = $request->sys_tmst_department_group_divisi_id;
            $data['sys_tmst_occupation_id'] = $request->sys_tmst_occupation_id;
            $data['sys_tmst_occupation_tree_id'] = $request->sys_tmst_occupation_tree_id;
            $data['sys_tmst_timeshift_id'] = $request->sys_tmst_timeshift_id;
            $data['journal_code'] = $request->journal_code;
            $data['sys_tmst_company_npwp_id'] = $request->sys_tmst_company_npwp_id;
            $data['sys_tmst_company_npp_id'] = $request->sys_tmst_company_npp_id;
            $data['sys_tmst_cost_center_id'] = $request->sys_tmst_cost_center_id;
            $data['username_employee'] = $request->username_employee;
            $data['mail_office'] = $request->mail_office;
            $data['absen_id'] = $request->absen_id;
            $data['sys_tmst_overtime_profile_id'] = $request->sys_tmst_overtime_profile_id;
            $data['pph_resume'] = $request->pph_resume;
            $data['pph_pajak'] = $request->pph_pajak;
            $data['jkk_code'] = $request->jkk_code;
            $data['jkk_value'] = $request->jkk_value;
            $data['bpjs_num'] = $request->bpjs_num;
            $data['bpjs_num_others'] = $request->bpjs_num_others;
            $data['marital_status_tax'] = $request->marital_status_tax;
            $data['fmode'] = $request->fmode;
            $data['history_status'] = $request->history_status;
            $data['main_opttax'] = $request->main_opttax;
            $data['jamsostek_code'] = $request->jamsostek_code;
            $data['fstatus'] = $request->fstatus;
            $data['sys_tmst_attendance_shift_id'] = $request->sys_tmst_attendance_shift_id;
            $data['custom1'] = $request->custom1;
            $data['custom2'] = $request->custom2;
            $data['custom3'] = $request->custom3;
            $data['custom4'] = $request->custom4;
            $data['custom7'] = $request->custom7;
            $data['description'] = $request->description;
            $data['createdate'] = now();
            $data['moduser'] = '2';
            $data['moddate'] = now();

            $insert = DB::table('sys_ttrs_employee_recruitment')->insert($data);

            $response = [
                'message'=>'Data successfully inserted.',
                'data'=> $insert
            ];

            return response()->json($response, Response::HTTP_CREATED);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
