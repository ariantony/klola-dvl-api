<?php

namespace App\Http\Controllers;

use Spatie\QueryBuilder\QueryBuilder;
use App\Models\EmployeeDB;
use App\Http\Helper\ResponseBuilder;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;


class EmployeeDBController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /*
        $employee = EmployeeDB::all();
        return response()->json([
            'pesan' => 'Data berhasil di ambil',
            'status' => 'sukses',
            'data' => $employee
        ]);
        */
        
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = EmployeeDB::paginate(15);

        return ResponseBuilder::result($status, $message, $data, $response_code);


    }

    public function createdate($tgl)
    {
        $employee = EmployeeDB::where('createdate','=', $tgl)->get();
        
        $rows = $employee->count();
        return response()->json([
            'pesan' => 'Data berhasil di ambil',
            'status' => 'sukses',
            'jumlah_data'=> $rows,
            'data' => $employee
        ]);
    }

    public function store(Request $request)
    {     
        
        try {
            if ($request->accepts(['application/json'])) {

                $rowsdata = array();
                $totalrows = 0;

                $validator = Validator::make($request->all(),[
                    'startwork' => ['required'],
                    'tax_month' => ['required'],
                    'sys_tmst_company_id' => ['required','numeric','min:1','max:9'],
                    'sys_tmst_level_id' => ['required','numeric','min:1'],
                    'sys_tmst_unit_id' => ['required','numeric','min:1','max:8'],
                    'sys_tmst_grade_id' => ['required','numeric','min:1','max:8'],
                    'sys_tmst_department_id' => ['required','numeric','min:1'],
                    'sys_tmst_department_group_divisi_id' => ['required','numeric','min:1','max:8'],
                    'sys_tmst_occupation_id' => ['required','min:1','max:32'],
                    'sys_tmst_cost_center_id' => ['required','min:1','max:32'],
                    'jkk_code' => ['required','min:2','max:32'],
                    'custom1' => ['required','min:2','max:32'],
                    'custom2' => ['required','min:2','max:32'],
                    'custom3' => ['required','min:2','max:32'],
                    'custom7' => ['required','min:2','max:32']
                ]);

                if($validator->fails()){
                    return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
                }


                $content = $request->json()->all();
                foreach($content as $row)
                {           
                    //---rows data
                    foreach($row as $key => $value) {
                
                
                        try {
                
                            $data = array();
                            $data[$key] = $value;

                            //$insert = DB::table('sys_ttrs_employee_recruitment')->insert($data);

                            $response = [
                                'message'=>'Data successfully inserted.',
                                'data'=> $insert
                            ];

                            return response()->json($response, Response::HTTP_CREATED);

                        } catch (QueryException $e) {
                            return response()->json([
                                'message' => "Failed" . $e->errorInfo
                            ]);
                        }                

                    }    
                }                
                //dd($rowsdata);
                
            } else {
                return response()->json([
                    'pesan' => "Failed Format Data..",
                    'status' => 'Gagal'
                ]);    
            }
        } catch (QueryException $e) {
            return response()->json([
                'pesan' => "Failed" . $e->errorInfo,
                'status' => 'Gagal'
            ]);
        }

    }

    public function search() {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = QueryBuilder::for(EmployeeArgenta::class)
        ->allowedFilters(['nip','pin','nama_lengkap','divisi','departemen','jabatan','area_kerja','tgl_mulai_kerja','status_pegawai','status_aktif','jenis_kelamin','email_kantor','createdate'])
        ->get();

        if (empty($data)){
            $message  = "Data kosong";
            return ResponseBuilder::result('False', $message, '[]', '404');
        }

        return ResponseBuilder::result($status, $message, $data, $response_code);

    }



}
