<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Models\HrDepartment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helper\ResponseBuilder;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class HrDepartmentController extends Controller
{
    public function index()
    {
<<<<<<< HEAD
            //$menu_list = HrDepartment::all('id','sys_tmst_company_id','departement_id','alias_code','name','parent','tid','cost_center','code_prod','group_id','description','createdate','moduser','moddate');

        // return json_encode($menu_list);
            $model = new HrDepartment();
            $tst = $model->getFillable();

            $slistdata = join("','",$tst);
            $listdata = sprintf("'%s'",$slistdata);

            //$data = $data->addSelect(DB::raw("'fakeValue' as fakeColumn"));
            $menu_list = HrDepartment::raw($listdata);



            dd($menu_list);
=======
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = HrDepartment::paginate(15);

        return ResponseBuilder::result($status, $message, $data, $response_code);
>>>>>>> 6cff4b34f4aa55cfdc67714be396cfe9cc33528b


        // $fillable = $hrDepartment->getFillable();
        // $listdata = "`".join("`,`",$fillable)."`";
        // $users = DB::table('sys_tmst_department')
        // ->select(DB::raw($listdata))
        // ->get();

        // return ($users);

    }

    public function adddivisi(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name' => ['required','min:2']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }


        // untuk id level root
        // SELECT departement_id
        // FROM sys_tmst_department
        // WHERE 1=1
        // and LENGTH(departement_id) = 2
        // ORDER BY id DESC
        // limit 1

        $maxid = (DB::table('sys_tmst_department')
            ->whereRaw('LENGTH(departement_id) = 2')
            ->orderBy('id','DESC')
            ->limit(1)
            ->value('departement_id')) + 1 ;

        $maxid_table = (DB::table('sys_tmst_department')
        ->selectRaw('MAX(id)')
        ->value('id')) + 1 ;

        try {

            $data = [];
            // $data['id'] = Str::uuid()->__toString();
            $data['id'] = $maxid_table;
            $data['sys_tmst_company_id'] = 1;
            $data['departement_id'] = $maxid;
            $data['alias_code'] = $request-> alias_code;
            $data['name'] = $request-> name;
            $data['parent'] = 'root';
            $data['cost_center'] =  $request-> cost_center;
            $data['group_id'] = 1; //$request-> group_id;
            $data['description'] = $request-> description;
            $data['createdate'] = now();
            $data['moduser'] = '2';
            $data['moddate'] = now();

            $insert = DB::table('sys_tmst_department')->insert($data);

            $response = [
                'message'=>'Data successfully inserted.',
                'status'=> $insert
            ];

            return response()->json($response, Response::HTTP_CREATED);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }

    }

    public function adddept(Request $request) {

        $validator = Validator::make($request->all(),[
            'name' => ['required','min:2'],
            'departement_id' => ['required','between:2,3']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        // untuk mencari id level 2
        // SELECT departement_id
        // FROM sys_tmst_department
        // WHERE 1=1
        // and LENGTH(departement_id) = 5
        // and LEFT(departement_id,2) = '11'
        // ORDER BY id DESC
        // limit 1

        $max_departement_id = (DB::table('sys_tmst_department')
        ->whereRaw('LENGTH(departement_id) = 5')
        ->whereRaw('LEFT(departement_id,2) = ?', $request->departement_id )
        ->orderBy('id','DESC')
        ->limit(1)
        ->value('departement_id')) + 1;

        $maxid_table = (DB::table('sys_tmst_department')
        ->selectRaw('MAX(id)')
        ->value('id')) + 1 ;

        // SELECT id
        // FROM sys_tmst_department
        // WHERE 1=1
		// 		and LENGTH(departement_id) = 2
        // and LEFT(departement_id,2) = 14
        // ORDER BY id DESC
        // limit 1

        $parent_id = DB::table('sys_tmst_department')
        ->whereRaw('LENGTH(departement_id) = 2')
        ->whereRaw('LEFT(departement_id,2) = ?', $request->departement_id )
        ->orderBy('id','DESC')
        ->limit(1)
        ->value('id');

        try {
            $data = [];
            $data['id'] = $maxid_table;
            $data['sys_tmst_company_id'] = 1;
            $data['departement_id'] = $max_departement_id ;
            $data['alias_code'] = $request-> alias_code;
            $data['name'] = $request-> name;
            $data['parent'] = $parent_id;
            $data['cost_center'] =  $request-> cost_center;
            $data['group_id'] = 1; //$request-> group_id;
            $data['description'] = $request-> description;
            $data['createdate'] = now();
            $data['moduser'] = '2';
            $data['moddate'] = now();

            $insert = DB::table('sys_tmst_department')->insert($data);

            $response = [
                'message'=>'Data successfully inserted.',
                'status'=> $insert
            ];

            return response()->json($response, Response::HTTP_CREATED);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }

    }

    public function addsection(Request $request) {

        $validator = Validator::make($request->all(),[
            'name' => ['required','min:2'],
            'departement_id' => ['required','between:5,6']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // untuk id level 3
        // SELECT departement_id
        // FROM sys_tmst_department
        // WHERE 1=1
        // and LENGTH(departement_id) = 8
        // and LEFT(departement_id,5) = '11001'
        // ORDER BY id DESC
        // limit 1

        $max_departement_id = (DB::table('sys_tmst_department')
        ->whereRaw('LENGTH(departement_id) = 8')
        ->whereRaw('LEFT(departement_id,5) = ?', $request->departement_id )
        ->orderBy('id','DESC')
        ->limit(1)
        ->value('departement_id')) + 1;

        $maxid_table = (DB::table('sys_tmst_department')
        ->selectRaw('MAX(id)')
        ->value('id')) + 1 ;

        // SELECT id
        // FROM sys_tmst_department
        // WHERE 1=1
		// 		and LENGTH(departement_id) = 5
        // and LEFT(departement_id,5) = 14009
        // ORDER BY id DESC
        // limit 1

        $parent_id = DB::table('sys_tmst_department')
        ->whereRaw('LENGTH(departement_id) = 5')
        ->whereRaw('LEFT(departement_id,5) = ?', $request->departement_id )
        ->orderBy('id','DESC')
        ->limit(1)
        ->value('id');

        try {
            $data = [];
            $data['id'] = $maxid_table;
            $data['sys_tmst_company_id'] = 1;
            $data['departement_id'] = $max_departement_id ;
            $data['alias_code'] = $request-> alias_code;
            $data['name'] = $request-> name;
            $data['parent'] = $parent_id;
            $data['cost_center'] =  $request-> cost_center;
            $data['group_id'] = 1; //$request-> group_id;
            $data['description'] = $request-> description;
            $data['createdate'] = now();
            $data['moduser'] = '2';
            $data['moddate'] = now();

            $insert = DB::table('sys_tmst_department')->insert($data);

            $response = [
                'message'=>'Data successfully inserted.',
                'status'=> $insert
            ];

            return response()->json($response, Response::HTTP_CREATED);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }

    }

    public function search() {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;

        $data = QueryBuilder::for(HrDepartment::class)
        ->allowedFilters(['name'])
        ->get();

        if (empty($data)){
            $message  = "Data kosong";
            return ResponseBuilder::result('False', $message, '[]', '404');
        }

        return ResponseBuilder::result($status, $message, $data, $response_code);

    }
}
