<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrDepartment extends Model
{
    use HasFactory;
    protected $connection = 'mysql_api';

    public $table = 'sys_tmst_department';

    protected $fillable = [
        'id',
        'sys_tmst_company_id',
        'departement_id',
        'alias_code',
        'name',
        'parent',
        'cost_center',
        'code_prod',
        'group_id',
        'description',
        'createdate',
        'moduser',
        'moddate'
    ];

    protected $guarded = [
        'tid'
    ];

    protected $hidden = [
        'sys_tmst_company_id',
        'code_prod',
        'tid'
    ];

    protected $casts = [
        'createdate' => 'datetime:Y-m-d H:i:s',
        'moddate' => 'datetime:Y-m-d H:i:s'
    ];
}
