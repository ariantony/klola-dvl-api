<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrPHK extends Model
{
    use HasFactory;
    protected $connection = 'mysql_api';

    public $table = 'sys_ttrs_employee_phk';

    protected $fillable = [
        'sys_tmst_company_id',
        'sys_ttrs_employee_id',
        'tmpid',
        'no_sk',
        'sys_tmst_alasan_phk_id',
        'apply_date_phk',
        'not_active_work_date',
        'not_active_work_date_actual',
        'last_employee_status',
        'fmode',
        'description',
        'createdate',
        'moduser',
        'moddate'
    ];

    protected $guarded = [
        'fstatus',
        'exit_clearance_confirm'
    ];

    protected $hidden = [
        'sys_tmst_company_id',
        'fstatus',
        'exit_clearance_confirm',
        'not_active_work_date_actual',
    ];

    protected $casts = [
        'createdate' => 'datetime:Y-m-d H:i:s',
        'moddate' => 'datetime:Y-m-d H:i:s',
        'apply_date_phk' => 'datetime:Y-m-d',
        'not_active_work_date' => 'datetime:Y-m-d'
    ];
}
