<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDB extends Model
{
    use HasFactory;
    protected $connection = 'mysql_api';

    public $table = 'sys_ttrs_employee';

    protected $guarded = [
        'tax_npwp_personal',
        'startdate',
        'enddate',
        'extenddate',
        'startwork',
        'tax_month',
        'no_sk',
        'sys_tmst_kpp_id',
        'sys_tmst_company_id',
        'sys_tmst_level_id',
        'sys_tmst_unit_id',
        'sys_tmst_grade_id',
        'sys_tmst_department_id',
        'sys_tmst_department_group_divisi_id',
        'sys_tmst_occupation_id',
        'sys_tmst_occupation_tree_id',
        'sys_tmst_timeshift_id',
        'journal_code',
        'sys_tmst_company_npwp_id',
        'sys_tmst_company_npp_id',
        'sys_tmst_cost_center_id',
        'username_employee',
        'sys_tmst_overtime_profile_id',
        'pph_resume',
        'pph_pajak',
        'jkk_code',
        'jkk_value',
        'bpjs_num',
        'bpjs_num_others',
        'history_status',
        'main_opttax',
        'jamsostek_code',
        'fstatus',
        'sys_tmst_attendance_shift_id',
        'employee_status',
        'last_employee_status',
        'resign_date',
        'resign_date_actual',
        'resign_reason',
        'kpp_movedate',
        'custom1',
        'custom2',
        'custom7'
    ];


    protected $hidden = [
        'employee_status',
        'last_employee_status',
        'pass',
        'pass2',
        'newpass',
        'custom5',
        'custom6',
        'custom8',
        'custom9',
        'custom10'
    ];


}
